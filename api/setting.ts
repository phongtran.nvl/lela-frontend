import {fetcher} from "@/utils/request";

export const getAllSetting = async () => {
	const {data} = await fetcher('setting', {
		headers: {
			'cache-control': 'public, max-age=86400'
		}
	});
	return data
}