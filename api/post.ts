import {
  GetLatestPostsFromRandomCategoriesDataType,
  SuggestionPostsDataType,
  PostDetailType, RelatedPostType, MostViewedPostType
} from "./types/post.type";
import { ResponseType } from "./types/response.type";
import {fetcher} from "@/utils/request";
import { cookies } from 'next/headers';

export const getLastestPostsFromRandomCategories = (): Promise<
  ResponseType<GetLatestPostsFromRandomCategoriesDataType>
> => {
  return fetcher('post/get-random-lastest-post');
};

export const getLastestPostsFromRandomCategory = (): Promise<
  ResponseType<GetLatestPostsFromRandomCategoriesDataType>
> => {
  return fetcher('post/get-random-category-lastest-posts');
};

export const getSuggestionPosts = (): Promise<
  ResponseType<SuggestionPostsDataType>
> => {
  try {
    return fetcher('post/get-suggestion-posts');
  } catch (error: any) {
    return error;
  }
};

export const getPostDetail = async (slug: string): Promise<ResponseType<PostDetailType>> => {
  try {
    const nextCookie = cookies();
    return await fetcher(`post/detail/${slug}`, {
      params: {
        token: nextCookie.get('access_token')?.value
      }
    });
  } catch (error: any) {
    return error;
  }
}

export const getRelatedPost = async (id: string, mainCategory:string, limit = 10): Promise<ResponseType<Array<RelatedPostType>>> => {
  try {
    return await fetcher(`post/get-related-post/${id}?main_category=${mainCategory}&limit=${limit}`);
  } catch (error: any) {
    return error;
  }
}

export const getMostViewed = async (limit = 5): Promise<ResponseType<Array<MostViewedPostType>>> => {
  try {
    return await fetcher(`post/get-most-viewed?limit=${limit}`);
  } catch (error: any) {
    return error;
  }
}