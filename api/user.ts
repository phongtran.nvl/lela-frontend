import {fetcher} from "@/utils";
import {RegisterUserType} from "./types/user.type";
import {CookieStorage} from "cookie-storage";
import get from "lodash/get";

const d = new Date();
d.setDate(new Date().getDate() + 7);
const cookie = new CookieStorage({
	expires: d,
});

export const registerUser = async (payload: RegisterUserType) => {
	try {
		return await fetcher({
			url: 'user/register',
			method: 'POST',
			data: payload,
		});
	} catch (error: any) {
		return error;
	}
}

export const loginByToken = async (token: string) => {
	try {
		return await fetcher({
			url: 'user/login-by-token',
			method: 'POST',
			data: {token},
		});
	} catch (error: any) {
		return error;
	}
}

export const loginByEmail = async (email: string) => {
	try {
		return await fetcher({
			url: 'user/login-by-email',
			method: 'POST',
			data: {email},
		});
	} catch (error: any) {
		return error;
	}
}

export const getUserProfile = async (token: string) => {
	try {
		return await fetcher({
			url: 'user/profile',
			method: 'GET',
			params: {token},
		});
	} catch (error: any) {
		return error;
	}
}


export const storageAccessToken = (accessToken: string) => {
	cookie.setItem('access_token', accessToken);
}

export const getAccessToken = () => {
	return cookie.getItem('access_token');
}

export const removeAccessToken = () => {
	document.cookie = 'access_token=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;';
}