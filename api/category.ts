import { ResponseType } from "./types/response.type";
import {CategoryType} from "./types/category.type";
import {fetcher} from "@/utils/request";

export const getCategories = async (): Promise<ResponseType<Array<CategoryType>>> => {
	try {
		return await fetcher('category/get-all-category');
	} catch (error: any) {
		return error;
	}
}