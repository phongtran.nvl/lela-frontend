export type WriterType = {
	fullName?: string;
	avatar?: string;
	description?: string;
}