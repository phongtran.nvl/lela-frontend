import {WriterType} from "./writer.type";
import {CategoryType} from "./category.type";

export type GetLatestPostsFromRandomCategoriesDataType = Array<{
  id: string;
  title: string;
  description?: string;
  slug: string;
  thumbnail: string;
  createdBy: WriterType;
  category: CategoryType;
}>;

export type PostDetailType = {
  id: string;
  title: string;
  slug: string;
  thumbnail: string;
  description: string;
  content: string;
  createdBy: WriterType;
  isLelaTalk: boolean;
  isShowComment: boolean;
}

export type SuggestionPostsDataType = Array<{
  id: string;
  title: string;
  slug: string;
  description?: string;
  status: string;
  thumbnail: string;
  categories?: Array<CategoryType>;
  mainCategory?: CategoryType;
  createdBy: WriterType;
  viewed: number;
  seo: {
    metaKeywords: string;
    metaDescription: string;
    isGoogleIndex: boolean;
    id: string;
  };
  is_recommed: boolean;
  isLelaTalk: boolean;
  createdAt: string;
  updatedAt: string;
  updatedBy?: WriterType;
}>;

export type RelatedPostType = {
  id: string;
  title: string;
  slug: string;
  thumbnail: string;
  createdBy: WriterType;
}

export type MostViewedPostType = RelatedPostType & {
  category: CategoryType;
}