export type ResponseType<TData> = {
  statusCode: number;
  error: boolean;
  message?: Array<any> | null;
  data?: TData;
};
