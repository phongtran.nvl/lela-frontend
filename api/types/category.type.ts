export type CategoryType = {
	id: string;
	title: string;
	slug: string;
	description?: string;
	content?: string;
}