export type RegisterUserType = {
	email: string;
	fullName: string;
	categories: Array<string>;
	userInformation?: any;
}