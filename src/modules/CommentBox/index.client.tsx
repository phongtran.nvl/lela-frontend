"use client";

import { useState, useCallback } from "react";
import Image, { StaticImageData } from "next/image";

import { Avatar, TextInput } from "@/components";
import { VIEWED_COMMENT_LIMIT } from "@/utils";

import HeartSvg from "@/assets/svg/Heart";
import arrowDownSvg from "@/assets/svg/arrowDown.svg";

import styles from "./index.module.scss";
import {Button} from "@mui/base";
import isEmpty from "lodash/isEmpty";
import defaultAvatar from "@/assets/png/default/avatar/defaultAvatar.png";

interface Comment {
  id?: string | number;
  avatarSrc?: StaticImageData | string;
  name: string;
  timestamp?: string;
  content: string;
  likes?: number;
}

const CommentBox = () => {
  const comments = [
    {
      id: 1,
      avatarSrc: defaultAvatar,
      name: "Alex Nguyễn",
      timestamp: "3 tiếng trước", // handle by moment.js later
      content: `Bài viết rất ý nghĩa. Cảm ơn tác giả. Mình kết câu:
Bản thân nghệ thuật có năng lực đánh thức, khơi gợi cái đẹp, cái thiện trong mỗi con người để những giá trị đó được bước ra cuộc sống. Nhiếp ảnh cho tôi niềm đam mê tích cực và lành mạnh để sống và sống có ý nghĩa hơn.`,
      likes: 50,
    },
    {
      id: 2,
      avatarSrc: defaultAvatar,
      name: "Alex Nguyễn",
      timestamp: "3 tiếng trước", // handle by moment.js later
      content: `Bài viết rất ý nghĩa. Cảm ơn tác giả. Mình kết câu:
Bản thân nghệ thuật có năng lực đánh thức, khơi gợi cái đẹp, cái thiện trong mỗi con người để những giá trị đó được bước ra cuộc sống. Nhiếp ảnh cho tôi niềm đam mê tích cực và lành mạnh để sống và sống có ý nghĩa hơn.`,
      likes: 50,
    },
    {
      id: 3,
      avatarSrc: defaultAvatar,
      name: "Alex Nguyễn",
      timestamp: "3 tiếng trước", // handle by moment.js later
      content: `Bài viết rất ý nghĩa. Cảm ơn tác giả. Mình kết câu:
Bản thân nghệ thuật có năng lực đánh thức, khơi gợi cái đẹp, cái thiện trong mỗi con người để những giá trị đó được bước ra cuộc sống. Nhiếp ảnh cho tôi niềm đam mê tích cực và lành mạnh để sống và sống có ý nghĩa hơn.`,
      likes: 50,
    },
    {
      id: 4,
      avatarSrc: defaultAvatar,
      name: "Alex Nguyễn",
      timestamp: "3 tiếng trước", // handle by moment.js later
      content: `Bài viết rất ý nghĩa. Cảm ơn tác giả. Mình kết câu:
Bản thân nghệ thuật có năng lực đánh thức, khơi gợi cái đẹp, cái thiện trong mỗi con người để những giá trị đó được bước ra cuộc sống. Nhiếp ảnh cho tôi niềm đam mê tích cực và lành mạnh để sống và sống có ý nghĩa hơn.`,
      likes: 50,
    },
    {
      id: 5,
      avatarSrc: defaultAvatar,
      name: "Alex Nguyễn",
      timestamp: "3 tiếng trước", // handle by moment.js later
      content: `Bài viết rất ý nghĩa. Cảm ơn tác giả. Mình kết câu:
Bản thân nghệ thuật có năng lực đánh thức, khơi gợi cái đẹp, cái thiện trong mỗi con người để những giá trị đó được bước ra cuộc sống. Nhiếp ảnh cho tôi niềm đam mê tích cực và lành mạnh để sống và sống có ý nghĩa hơn.`,
      likes: 50,
    },
    {
      id: 6,
      avatarSrc: defaultAvatar,
      name: "Alex Nguyễn",
      timestamp: "3 tiếng trước", // handle by moment.js later
      content: `Bài viết rất ý nghĩa. Cảm ơn tác giả. Mình kết câu:
Bản thân nghệ thuật có năng lực đánh thức, khơi gợi cái đẹp, cái thiện trong mỗi con người để những giá trị đó được bước ra cuộc sống. Nhiếp ảnh cho tôi niềm đam mê tích cực và lành mạnh để sống và sống có ý nghĩa hơn.`,
      likes: 50,
    },
  ];

  const [comment, setComment] = useState('');

  const [viewedLimit, setViewedLimit] = useState<number>(VIEWED_COMMENT_LIMIT);

  const isShowViewMoreButton = useCallback(() => {
    return viewedLimit < comments.length;
  }, [viewedLimit, comments]);

  return (
    <div className={styles.commentBox}>
      <div className={styles.commentTopSection}>
        <div className={styles.titleWithCount}>Comment ({comments.length})</div>
        <TextInput
          multiline
          placeholder="Bạn nghĩ gì về bài viết?"
          className={styles.textAreaComment}
          wrapperClassName={styles.textAreaCommentWrapper}
          onChange={(e) => setComment(e.target.value)}
        />
        <Button className={styles.buttonPrimary} disabled={isEmpty(comment)}>Bình luận</Button>
      </div>

      {comments.length > 0 && (
        <div className={styles.allCommentsSection}>
          {comments.slice(0, viewedLimit).map((comment) => (
            <div key={comment.id} className={styles.commentWrapper}>
              <div className={styles.commentInfoSection}>
                <Avatar
                  imgSrc={comment.avatarSrc}
                  className={styles.commentAvatarWrapper}
                />
                <div className={styles.nameAndTimestamp}>
                  <div className={styles.commentName}>{comment.name}</div>
                  <div className={styles.commentTimestamp}>
                    {comment.timestamp}
                  </div>
                </div>
              </div>
              <div className={styles.commentContentSection}>
                {comment.content}
              </div>
              <div className={styles.commentLikeSection}>
                <div className={styles.icon}>
                  <HeartSvg className={styles.heartIcon1} />
                </div>
                <div className={styles.likeCount}>{comment.likes}</div>
              </div>
            </div>
          ))}
        </div>
      )}
      {isShowViewMoreButton() && (
        <div className={styles.viewMoreButtonWrapper}>
          <div
            className={styles.ghost}
            onClick={() => setViewedLimit(comments.length)}
          >
            <div className={styles.iconLeft}>
              <Image
                className={styles.arrowDownIcon1}
                alt="arrow down icon"
                src={arrowDownSvg}
              />
            </div>
            <div className={styles.button}>Xem thêm</div>
          </div>
        </div>
      )}
    </div>
  );
};

export default CommentBox;
