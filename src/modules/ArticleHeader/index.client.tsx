"use client";

import {useEffect, useState} from "react";
import Image from "next/image";
import Link from "next/link";

import { WriterType } from "../../../api/types/writer.type";

import SharePopover from "../SharePopover/index.client";

import CommentSvg from "@/assets/svg/Comment";
import HeartSvg from "@/assets/svg/Heart";
import ShareSvg from "@/assets/svg/Share";
import BookmarkSvg from "@/assets/svg/Bookmark";
import articleDefault from "@/assets/png/default/article/blogDefault.png";

import styles from "./index.module.scss";

type Props = {
  title: string;
  description: string;
  createdBy: WriterType;
  thumbnail: string;
  categoryName: string;
};

const ArticleHeader = ({
  title,
  description,
  createdBy,
  thumbnail,
  categoryName,
}: Props) => {
  const [contentReady, setContentReady] = useState<boolean>(false);
  const [popoverOpen, setPopoverOpen] = useState<boolean>(false);

  useEffect(() => {
    setContentReady(true);
  }, []);

  if (!contentReady) return null;

  return (
    <div className={styles.articleHeader}>
      <div className={styles.content}>
        <div className={styles.linkParent}>
          <div className={styles.link}>
            <div className={styles.link1}>{categoryName}</div>
          </div>
          <h1 className={styles.nhnNgySch1}>{title}</h1>
          <div className={styles.lelaJournalGi}>{description}</div>
          <div className={styles.biJasonLa1}>Bởi {createdBy.fullName}</div>
        </div>
        <div className={styles.frameGroup}>
          <div className={styles.frameDiv}>
            <div className={styles.iconParent}>
              <div className={styles.icon}>
                <HeartSvg className={styles.heartIcon1} />
              </div>
              <div className={styles.label}>34</div>
            </div>
            <Link className={styles.iconParent} href="#comment-section">
              <div className={styles.icon}>
                <CommentSvg className={styles.heartIcon1} />
              </div>
              <div className={styles.label}>8</div>
            </Link>
            <div className={styles.iconParentRelative}>
              <div
                className={styles.icon}
                onClick={() => setPopoverOpen(!popoverOpen)}
              >
                <ShareSvg className={styles.heartIcon1} />
              </div>
              <div className={styles.label}>Share</div>
              {popoverOpen && <SharePopover />}
            </div>
          </div>
          <div className={styles.iconParent}>
            <div className={styles.icon}>
              <BookmarkSvg className={styles.heartIcon1} />
            </div>
            <div className={styles.label}>Save</div>
          </div>
        </div>
      </div>
      <Image
        className={styles.image}
        src={thumbnail || articleDefault}
        width={600}
        height={385}
        alt="default image"
      />
    </div>
  );
};

export default ArticleHeader;
