import dynamic from "next/dynamic";

import defaultArticleBlogImg from "@/assets/png/default/article/articleBlogDefaultImg.png";

import styles from "./index.module.scss";

const TextDisplay = dynamic(
  () => import("@/components/TextDisplay/index.client"),
  {
    suspense: true,
  }
);

const DetailedImage = dynamic(
  () => import("@/components/DetailedImage/index.client"),
  {
    suspense: true,
  }
);

const ArticleSpecialSection = dynamic(
  () => import("@/modules/ArticleSpecialSection/index.client"),
  {
    suspense: true,
  }
);

const ArticleSeriesPromotion = dynamic(
  () => import("@/modules/ArticleSeriesPromotion/index.client"),
  {
    suspense: true,
  }
);

const EXAMPLE_TITLE_TEXT = `Những câu chuyện là nền tảng gắn kết các bộ não với nhau`;
const EXAMPLE_LONG_TITLE_TEXT = `Kể chuyện và đọc sách mang lại những lợi ích gì cho sự phát triển của trẻ nhỏ?`;
const EXAMPLE_TEXT =
  'Từ một người chụp ảnh dạo thích đi đây đó để tìm kiếm chân-thiện-mỹ trong đời sống thường nhật, nhiếp ảnh gia Lê Hồng Linh đã trở thành Chủ tịch Hội đồng Nghệ thuật của Hội Nhiếp ảnh Việt Nam, được Liên đoàn Nhiếp ảnh Nghệ thuật Quốc tế FIAP phong tặng tước hiệu Bậc thầy (Master) năm 2011 và là nhiếp ảnh gia Việt Nam đầu tiên được Hội Nhiếp ảnh Mỹ - PSA vinh danh Hạng Nhất thế giới thể loại ảnh in đen-trắng vào năm 2005. Đó là những giấc mơ rất đẹp đã thành hiện thực, không chỉ với riêng nghệ sĩ nhiếp ảnh Lê Hồng Linh, mà còn của rất nhiều người làm nghệ thuật chân chính. Nhưng con đường phụng sự của người nghệ sĩ không chỉ dừng lại ở những danh hiệu hay giải thưởng, mà còn ở những câu chuyện "chữ Tâm kia mới bằng ba chữ Tài".';

const EXAMPLE_SHORT_QUOTE = `“Báo chí và nghệ thuật đều mở cho tôi cánh cửa để khám phá và nhận thức cuộc sống sâu sắc hơn. Tôi vẫn thích chụp những gì chân chất, mộc mạc nhưng phải có cảm xúc trong ấy. Có lẽ vì vậy mà tôi khá thành công với đề tài chân dung con người.”`;
const EXAMPLE_LONG_QUOTE = `“Báo chí và nghệ thuật đều mở cho tôi cánh cửa để khám phá và nhận thức cuộc sống sâu sắc hơn. Tôi vẫn thích chụp những gì chân chất, mộc mạc nhưng phải có cảm xúc trong ấy. Có lẽ vì vậy mà tôi khá thành công với đề tài chân dung con người.”`;
const EXAMPLE_IMAGE_DESCRIPTION = `"Nhà nhiếp ảnh chụp ảnh bằng cái đầu, chứ không chỉ bằng máy ảnh" (Tranh của họa sĩ NOP vẽ tặng nhiếp ảnh gia Lê Hồng Linh)"`;
const EXAMPLE_IMAGE_DESCRIPTION_2 = `Tác phẩm "Hồn nhiên" - Ảnh chụp trắng đen của nhiếp ảnh gia Lê Hồng Linh (Ảnh: NVCC)`;

interface RegularBlogProps {
  content?: {};
}

const RegularBlog = (props: RegularBlogProps) => {
  const { content } = props;

  return (
    <>
      <div className={styles.topParagraph}>
        <TextDisplay className={styles.titleText}>
          {EXAMPLE_TITLE_TEXT}
        </TextDisplay>
        <div className={styles.miniParagraph}>
          <TextDisplay className={styles.normalText}>
            Vào năm 2005, tôi được xác lập kỷ lục là nghệ sĩ nhiếp ảnh đạt nhiều
            giải thưởng nhất Việt Nam. Tuy nhiên, khi giữ cương vị Chủ tịch Hội
            đồng nghệ thuật Hội Nhiếp ảnh Việt Nam vào năm 2010, tôi bắt đầu suy
            nghĩ nhiều hơn về chuyện danh hiệu và giải thưởng. Đã 500 giải
            thưởng quốc tế rồi, bây giờ có 600, 700 hay thậm chí 1.000 giải thì
            vẫn chẳng mang lại ý nghĩa gì hơn. Bây giờ ở cương vị mới, mình phải
            dành thời gian để chia sẻ kinh nghiệm, truyền lửa, lan toả tình yêu
            với các tay máy trẻ Việt Nam để thêm nhiều nhà nhiếp ảnh đạt giải
            quốc tế thì số lượng giải thưởng sẽ lớn hơn và giá trị hơn so với
            chỉ mình tôi.
          </TextDisplay>
          <TextDisplay className={styles.normalText}>
            Gần đây, một số tổ chức nhiếp ảnh mời tôi làm giám khảo quốc tế. Nếu
            mình không tham dự các cuộc thi ảnh trên thế giới thì sẽ khó nắm bắt
            được các xu hướng sáng tác ảnh lẫn phương thức chấm giải thưởng hiện
            nay như thế nào. Do đó, tôi đã thi ảnh trở lại. Tính riêng năm nay,
            tôi đạt 200 giải thưởng ở 12 quốc gia (Mỹ, Hồng Kông, đảo Síp, Ý,
            Thuỵ Sĩ, Hungary, Rumani, Ấn Độ…). Trong đó, đặc biệt có 61 giải ảnh
            báo chí với 28 huy chương vàng.
          </TextDisplay>
        </div>
      </div>

      <DetailedImage
        src={defaultArticleBlogImg}
        imgClassName={styles.imageWithDetail}
        description={EXAMPLE_IMAGE_DESCRIPTION}
      />

      <div className={styles.paragraph}>
        <TextDisplay className={styles.titleText}>
          {EXAMPLE_LONG_TITLE_TEXT}
        </TextDisplay>
        <div className={styles.miniParagraph}>
          <TextDisplay className={styles.numericTitleText}>
            <TextDisplay className={styles.numericText}>01 –</TextDisplay>{" "}
            <TextDisplay className={styles.boldText}>
              Phát triển nhận thức
            </TextDisplay>
          </TextDisplay>

          <TextDisplay className={styles.normalText}>
            Gần đây, một số tổ chức nhiếp ảnh mời tôi làm giám khảo quốc tế. Nếu
            mình không tham dự các cuộc thi ảnh trên thế giới thì sẽ khó nắm bắt
            được các xu hướng sáng tác ảnh lẫn phương thức chấm giải thưởng hiện
            nay như thế nào. Do đó, tôi đã thi ảnh trở lại. Tính riêng năm nay,
            tôi đạt 200 giải thưởng ở 12 quốc gia (Mỹ, Hồng Kông, đảo Síp, Ý,
            Thuỵ Sĩ, Hungary, Rumani, Ấn Độ…). Trong đó, đặc biệt có 61 giải ảnh
            báo chí với 28 huy chương vàng.
          </TextDisplay>
        </div>
        <div className={styles.miniParagraph}>
          <TextDisplay className={styles.numericTitleText}>
            <TextDisplay className={styles.numericText}>02 –</TextDisplay>{" "}
            <TextDisplay className={styles.boldText}>
              Phát triển năng lực cảm xúc, cảm giác gắn bó
            </TextDisplay>
          </TextDisplay>

          <TextDisplay className={styles.normalText}>
            Gần đây, một số tổ chức nhiếp ảnh mời tôi làm giám khảo quốc tế. Nếu
            mình không tham dự các cuộc thi ảnh trên thế giới thì sẽ khó nắm bắt
            được các xu hướng sáng tác ảnh lẫn phương thức chấm giải thưởng hiện
            nay như thế nào. Do đó, tôi đã thi ảnh trở lại. Tính riêng năm nay,
            tôi đạt 200 giải thưởng ở 12 quốc gia (Mỹ, Hồng Kông, đảo Síp, Ý,
            Thuỵ Sĩ, Hungary, Rumani, Ấn Độ…). Trong đó, đặc biệt có 61 giải ảnh
            báo chí với 28 huy chương vàng.
          </TextDisplay>
        </div>
      </div>

      <div className={styles.specialSection}>
        <ArticleSpecialSection shortQuote={EXAMPLE_SHORT_QUOTE} />
      </div>

      <div className={styles.paragraph}>
        <div className={styles.miniParagraph}>
          <TextDisplay className={styles.normalText}>
            Gần đây, một số tổ chức nhiếp ảnh mời tôi làm giám khảo quốc tế. Nếu
            mình không tham dự các cuộc thi ảnh trên thế giới thì sẽ khó nắm bắt
            được các xu hướng sáng tác ảnh lẫn phương thức chấm giải thưởng hiện
            nay như thế nào. Do đó, tôi đã thi ảnh trở lại. Tính riêng năm nay,
            tôi đạt 200 giải thưởng ở 12 quốc gia (Mỹ, Hồng Kông, đảo Síp, Ý,
            Thuỵ Sĩ, Hungary, Rumani, Ấn Độ…). Trong đó, đặc biệt có 61 giải ảnh
            báo chí với 28 huy chương vàng.
          </TextDisplay>
        </div>
      </div>

      <ArticleSeriesPromotion
        className={styles.articleSeriesPromotionWrapper}
      />

      <div className={styles.paragraph}>
        <div className={styles.miniParagraph}>
          <TextDisplay className={styles.numericTitleText}>
            <TextDisplay className={styles.numericText}>03 –</TextDisplay>{" "}
            <TextDisplay className={styles.boldText}>
              Phát triển năng lực cảm xúc, cảm giác gắn bó
            </TextDisplay>
          </TextDisplay>

          <TextDisplay className={styles.normalText}>
            Gần đây, một số tổ chức nhiếp ảnh mời tôi làm giám khảo quốc tế. Nếu
            mình không tham dự các cuộc thi ảnh trên thế giới thì sẽ khó nắm bắt
            được các xu hướng sáng tác ảnh lẫn phương thức chấm giải thưởng hiện
            nay như thế nào. Do đó, tôi đã thi ảnh trở lại. Tính riêng năm nay,
            tôi đạt 200 giải thưởng ở 12 quốc gia (Mỹ, Hồng Kông, đảo Síp, Ý,
            Thuỵ Sĩ, Hungary, Rumani, Ấn Độ…). Trong đó, đặc biệt có 61 giải ảnh
            báo chí với 28 huy chương vàng.
          </TextDisplay>
        </div>
      </div>
    </>
  );
};

export default RegularBlog;
