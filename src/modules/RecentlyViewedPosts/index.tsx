import type { NextPage } from "next";
import Image from "next/image";

import defaultImage from "@/assets/png/default/tamLyDefault.png";
import vector3StrokeSvg from "@/assets/svg/vector3Stroke.svg";

import styles from "./index.module.scss";
import {getMostViewed} from "../../../api/post";
import get from "lodash/get";
import clsx from "clsx";
import Link from "next/link";
import {getPostDetailPath} from "@/utils/path";

const RecentlyViewedPosts: NextPage = async () => {
  const mostViewedPosts = await getMostViewed();

  return (
    get(mostViewedPosts, "data", []).length > 0 && (
      <div className={styles.recentlyViewedPosts}>
        <div className={styles.graphicKv}>
          <Image
            className={styles.vector3Stroke1}
            alt="stroke line"
            src={vector3StrokeSvg}
          />
        </div>
        <div className={styles.xuHng}>
          <div className={styles.miNgiU1}>Mọi người đều đọc</div>
          <div className={styles.trendingParent}>
            {
              get(mostViewedPosts, "data", []).map((post, index) => {
                const trendingClass = clsx(styles[`trending${index+1}`])
                return (
                  <div key={post.id} className={trendingClass}>
                    <div className={styles.div}>{`0${index+1}.`}</div>
                    {
                      index === 0 && (
                        <Link href={getPostDetailPath(post.slug)}>
                          <Image
                            className={styles.trendingChild}
                            alt={post.title}
                            src={get(post, 'thumbnail', defaultImage)}
                            width={250}
                            height={250}
                          />
                        </Link>
                      )
                    }
                    <div className={styles.linkParent}>
                      <div className={styles.link}>
                        <div className={styles.link1}>
                          {post.category.title}
                        </div>
                      </div>
                      <b className={styles.thcHChuyn5}>
                        <Link href={getPostDetailPath(post.slug)}>
                          {post.title}
                        </Link>
                      </b>
                    </div>
                  </div>
                )
              })
            }
          </div>
        </div>
      </div>
    )
  );
};

export default RecentlyViewedPosts;
