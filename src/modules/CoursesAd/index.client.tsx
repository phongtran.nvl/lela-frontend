"use client";
import Image from "next/image";
import checkmarkCircle from "@/assets/svg/checkmarkCircle.svg";
import styles from "./index.module.scss";

const CoursesAd = () => {
  return (
    <div className={styles.coursesAd}>
      <div className={styles.frameParent}>
        <div className={styles.chnGiKhoHcParent}>
          <div className={styles.chnGiKho1}>Đăng ký nhận tin</div>
          <b className={styles.dnhChoMi1}>Nếu bạn không muốn bỏ lỡ những
            bài viết thú vị</b>
        </div>
        <div className={styles.mKhoTim1}>
          Mở khoá tiềm năng tri thức của bạn với LeLa Membership để có được
          cộng đồng và đón nhận các khoá học giúp bạn xây dựng cuộc sống màu
          sắc hơn.
        </div>
      </div>
      <div className={styles.cardParent}>
        <div className={styles.card}>
          <div className={styles.basicParent}>
            <div className={styles.basic}>Basic</div>
            <div className={styles.thng}>
              <span className={styles.span}>{`200.000 `}</span>
              <span className={styles.thng1}>/ tháng</span>
            </div>
            <div className={styles.phHpNht3}>
              Phù hợp nhất để khởi đầu và tham khảo các khoá học
            </div>
          </div>
          <div className={styles.subsciptionContentParent}>
            <div className={styles.subsciptionContent}>
              <Image
                className={styles.checkmarkCircleIcon9}
                alt="checkmark"
                src={checkmarkCircle}
              />
              <div className={styles.ttCCc3}>Tất cả các khoá học cơ bản</div>
            </div>
            <div className={styles.subsciptionContent1}>
              <Image
                className={styles.checkmarkCircleIcon9}
                alt="checkmark"
                src={checkmarkCircle}
              />
              <div className={styles.ttCCc3}>
                10 bài đọc nâng cao mỗi tháng
              </div>
            </div>
            <div className={styles.subsciptionContent1}>
              <Image
                className={styles.checkmarkCircleIcon9}
                alt="checkmark"
                src={checkmarkCircle}
              />
              <div className={styles.ttCCc3}>6 tiếng podcast mỗi tuần</div>
            </div>
          </div>
          <div className={styles.secondary}>
            <div className={styles.button}>Đăng ký ngay</div>
          </div>
        </div>
        <div className={styles.card1}>
          <div className={styles.exploreParent}>
            <div className={styles.explore}>Explore</div>
            <div className={styles.thng}>
              <span className={styles.span}>{`350.000 `}</span>
              <span className={styles.thng1}>/ tháng</span>
            </div>
            <div className={styles.phHpNht3}>
              Phù hợp nhất để khởi đầu và tham khảo các khoá học
            </div>
          </div>
          <div className={styles.subsciptionContentParent}>
            <div className={styles.subsciptionContent}>
              <Image
                className={styles.checkmarkCircleIcon9}
                alt="checkmark"
                src={checkmarkCircle}
              />
              <div className={styles.ttCCc3}>Tất cả các khoá học cơ bản</div>
            </div>
            <div className={styles.subsciptionContent1}>
              <Image
                className={styles.checkmarkCircleIcon9}
                alt="checkmark"
                src={checkmarkCircle}
              />
              <div className={styles.ttCCc3}>
                10 bài đọc nâng cao mỗi tháng
              </div>
            </div>
            <div className={styles.subsciptionContent1}>
              <Image
                className={styles.checkmarkCircleIcon9}
                alt="checkmark"
                src={checkmarkCircle}
              />
              <div className={styles.ttCCc3}>6 tiếng podcast mỗi tuần</div>
            </div>
          </div>
          <div className={styles.secondary}>
            <div className={styles.button}>Đăng ký ngay</div>
          </div>
        </div>
        <div className={styles.card}>
          <div className={styles.basicParent}>
            <div className={styles.expertParent}>
              <div className={styles.basic}>Expert</div>
              <div className={styles.thng}>
                <span className={styles.span}>{`500.000 `}</span>
                <span className={styles.thng1}>/ tháng</span>
              </div>
            </div>
            <div className={styles.phHpNht3}>
              Phù hợp nhất để khởi đầu và tham khảo các khoá học
            </div>
          </div>
          <div className={styles.subsciptionContentParent}>
            <div className={styles.subsciptionContent}>
              <Image
                className={styles.checkmarkCircleIcon9}
                alt="checkmark"
                src={checkmarkCircle}
              />
              <div className={styles.ttCCc3}>Tất cả các khoá học cơ bản</div>
            </div>
            <div className={styles.subsciptionContent1}>
              <Image
                className={styles.checkmarkCircleIcon9}
                alt="checkmark"
                src={checkmarkCircle}
              />
              <div className={styles.ttCCc3}>
                10 bài đọc nâng cao mỗi tháng
              </div>
            </div>
            <div className={styles.subsciptionContent1}>
              <Image
                className={styles.checkmarkCircleIcon9}
                alt="checkmark"
                src={checkmarkCircle}
              />
              <div className={styles.ttCCc3}>6 tiếng podcast mỗi tuần</div>
            </div>
          </div>
          <div className={styles.secondary}>
            <div className={styles.button}>Đăng ký ngay</div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default CoursesAd;
