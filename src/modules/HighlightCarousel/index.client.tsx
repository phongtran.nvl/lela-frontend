"use client";

import { PropsWithChildren, useRef } from "react";
import Slider from "react-slick";

import ArrowLeftSvg from "@/assets/svg/ArrowLeft";
import ArrowRightSvg from "@/assets/svg/ArrowRight";

import styles from "./index.module.scss";

const DEFAULT_SETTINGS = {
  infinite: true,
  autoplay: true,
  pauseOnHover: true,
  swipeToSlide: true,
  slidesToScroll: 2,
  rows: 1,
  variableWidth: true,
};

interface HighlightCarouselProps extends PropsWithChildren {}

const HighlightCarousel = (props: HighlightCarouselProps) => {
  const { children, ...rest } = props;

  const sliderRef = useRef<any>(null);

  return (
    <>
      <div className={styles.highlightCarousel}>
        <div className={styles.sectionHeading}>
          <div className={styles.headingTitle1}>Weekly highlight</div>
          <div className={styles.main}>
            <b className={styles.headingTitle2}>Tuần này</b>
            <div className={styles.line2}>
              <div className={styles.line2Inner}>
                <div className={styles.frameChild} />
              </div>
              <b className={styles.headingTitle2}>có gì hay?</b>
            </div>
          </div>
        </div>
        <div className={styles.iconParent}>
          <div
            className={styles.icon}
            onClick={() => sliderRef.current?.slickPrev()}
          >
            <ArrowLeftSvg color="#FFFFFF" />
          </div>
          <div
            className={styles.icon}
            onClick={() => sliderRef.current?.slickNext()}
          >
            <ArrowRightSvg color="#FFFFFF" />
          </div>
        </div>
      </div>
      <div className={styles.cardParentMobile}>{children}</div>
      <Slider
        {...DEFAULT_SETTINGS}
        {...rest}
        className={styles.cardParentDesktop}
        ref={sliderRef}
      >
        {children}
      </Slider>
    </>
  );
};

export default HighlightCarousel;
