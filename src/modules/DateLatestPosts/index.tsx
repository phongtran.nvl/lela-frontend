import type { NextPage } from "next";
import Image from "next/image";
import Link from "next/link";

import { getLastestPostsFromRandomCategories } from "../../../api/post";
import { getPostDetailPath } from "@/utils/path";

import defaultThumbnail from "../../../public/default-thumbnail.svg";

import styles from "./index.module.scss";

const DateLatestPosts: NextPage = async () => {
  const { data: posts } = await getLastestPostsFromRandomCategories();

  return (
    posts && (
      <div className={styles.dateLatestPosts}>
        <div className={styles.sectionHeading}>
          <div className={styles.title1}>Mới nhất</div>
          <div className={styles.main}>
            <b className={styles.title2}>Những bài viết</b>
            <div className={styles.line2}>
              <div className={styles.line2Inner}>
                <div className={styles.frameChild} />
              </div>
              <b className={styles.title2}>trong ngày</b>
            </div>
          </div>
        </div>
        <div className={styles.cardParent}>
          {posts.slice(0, 4).map((post, index) => (
            <div className={styles.card} key={index}>
              <Link
                href={getPostDetailPath(post.slug)}
                className={styles.fullWidth}
              >
                <Image
                  className={styles.imageIcon}
                  alt={`default image ${++index}`}
                  src={post.thumbnail ? post.thumbnail : defaultThumbnail}
                  width={600}
                  height={600}
                />
              </Link>
              <div className={styles.content}>
                <div className={styles.link}>
                  <div className={styles.link1}>{post.category.title}</div>
                </div>
                <div className={styles.title}>
                  <Link href={getPostDetailPath(post.slug)}>{post.title}</Link>
                </div>
                <div className={styles.biJasonLa7}>
                  Bởi {post.createdBy.fullName}
                </div>
              </div>
            </div>
          ))}
        </div>
        {posts.length > 4 && (
          <div className={styles.cardGroup}>
            {posts.slice(4, 7).map((post, index) => (
              <div className={styles.card} key={index}>
                <Image
                  className={styles.imageIcon}
                  alt={`default image ${++index}`}
                  src={post.thumbnail ? post.thumbnail : defaultThumbnail}
                  width={600}
                  height={600}
                />
                <div className={styles.content}>
                  <div className={styles.link}>
                    <div className={styles.link1}>{post.category.title}</div>
                  </div>
                  <div className={styles.title4}>{post.title}</div>
                  <div className={styles.biJasonLa7}>
                    Bởi {post.createdBy.fullName}
                  </div>
                </div>
              </div>
            ))}
          </div>
        )}
      </div>
    )
  );
};

export default DateLatestPosts;
