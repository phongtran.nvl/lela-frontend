//#region Home components
import Highlight from "./Highlight/index.client";
import RecentlyViewedPosts from "./RecentlyViewedPosts";
import DateLatestPosts from "./DateLatestPosts";
import SubscribeBox from "./SubscribeBox/index.client";
import CategoryLatestPosts from "./CategoryLatestPosts";
import Series from "./Series";
import EditorRecommendation from "./EditorRecommendation";
import CoursesAd from "./CoursesAd/index.client";
import SignInModal from "@/modules/AuthModal/index.client";
//#endregion

//#region Article components
import ArticleHeader from "./ArticleHeader/index.client";
import ArticleSpecialSection from "./ArticleSpecialSection/index.client";
import ArticleSeriesPromotion from "./ArticleSeriesPromotion/index.client";
import SharePopover from "./SharePopover/index.client";
import InterviewBlog from "./InterviewBlog/index.client";
import RegularBlog from "./RegularBlog/index.client";
import ArticleRecentlyViewed from "./ArticleRecentlyViewed/index.client";
import LalaNews from "./LalaNews/index.client";
import CommentBox from "./CommentBox/index.client";
//#endregion

export {
  Highlight,
  RecentlyViewedPosts,
  DateLatestPosts,
  SubscribeBox,
  CategoryLatestPosts,
  Series,
  EditorRecommendation,
  CoursesAd,
  SignInModal,
  ArticleHeader,
  ArticleSpecialSection,
  ArticleSeriesPromotion,
  SharePopover,
  InterviewBlog,
  RegularBlog,
  ArticleRecentlyViewed,
  LalaNews,
  CommentBox,
};
