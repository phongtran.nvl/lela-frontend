"use client";

import Image from "next/image";

import { Icon } from "@/components";
import arrowRightSvg from "@/assets/svg/arrowRight.svg";

import defaultImage4 from "@/assets/png/default/dateLatestPosts/defaultImage4.png";

import styles from "./index.module.scss";
import {RelatedPostType} from "../../../api/types/post.type";
import Link from "next/link";
import {getPostDetailPath} from "@/utils";


type Props = {
  relatedPosts: Array<RelatedPostType> | []
}

const LalaNews = ({relatedPosts}: Props) => {
  return (
    <div className={styles.lalaNews}>
      <div className={styles.headingSectionWrapper}>
        <div className={styles.sectionHeading}>
          <div className={styles.khngThB1}>Những bài tương tự từ</div>
          <div className={styles.rnLuynParent}>
            <b className={styles.rnLuyn}>LeLa News</b>
            <div className={styles.rectangleWrapper}>
              <div className={styles.frameChild} />
            </div>
          </div>
        </div>
        <div className={styles.ghost}>
          <Icon
            className={styles.iconLeft1}
            iconClassName={styles.iconLeft2}
            alt="arrow right"
            src={arrowRightSvg}
          />
          <div className={styles.button}>Xem tất cả</div>
        </div>
      </div>
      <div className={styles.cardParent}>
        {
          relatedPosts.map((item) => {
            return (
              <div key={item.id} className={styles.card}>
                <Link href={getPostDetailPath(item.slug)}>
                  <Image
                    className={styles.imageIcon}
                    alt="default image 4"
                    src={item.thumbnail || defaultImage4}
                    width={100}
                    height={100}
                  />
                </Link>
                <div className={styles.content}>
                  <div className={styles.title}>
                    <p className={styles.nhNghinCu}>
                      <Link href={getPostDetailPath(item.slug)}>
                        {item.title}
                      </Link>
                    </p>
                  </div>
                  <div className={styles.biJasonLa4}>Bởi {item.createdBy.fullName}</div>
                </div>
              </div>
            )
          })
        }
      </div>
    </div>
  );
};

export default LalaNews;
