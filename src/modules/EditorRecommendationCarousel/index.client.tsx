"use client";

import {PropsWithChildren, useEffect, useRef, useState} from "react";
import Image from "next/image";
import Slider from "react-slick";

import { SuggestionPostsDataType } from "../../../api/types/post.type";

import defaultImage from "@/assets/png/default/highlight/highlightDefault2.png";
import styles from "./index.module.scss";
import Link from "next/link";
import {getPostDetailPath} from "@/utils";

interface EditorRecommendationCarouselProps extends PropsWithChildren {
  data: SuggestionPostsDataType;
}

const DEFAULT_SETTINGS = {
  infinite: true,
  autoplay: true,
  pauseOnHover: true,
  swipeToSlide: true,
  slidesToScroll: 1,
  rows: 1,
  arrows: false,
};

const EditorRecommendationCarousel = (
  props: EditorRecommendationCarouselProps
) => {
  const { children, data, ...rest } = props;
  const [sliders, setSliders] = useState(Array<any>)

  useEffect(() => {
    if (data) {
      data.map((item) => {
        setSliders((prev) => {
          return [...prev, {
            id: item.id,
            imgSrc: item.thumbnail,
            title: item.title,
            description: item.description,
            slug: item.slug,
          }]
        })
      })
    }
  }, [setSliders, data]);

  const sliderRef = useRef<any>(null);

  return (
    <Slider
      {...DEFAULT_SETTINGS}
      {...rest}
      ref={sliderRef}
      className={styles.editorRecommendationCarousel}
    >
      {sliders.map((child) => (
        <div key={child.id}>
          <Link href={getPostDetailPath(child.slug)}>
          <Image src={child.imgSrc} alt="img" className={styles.imgWrapper} width={450} height={450} />
          <div className={styles.content}>
            <div className={styles.contentTitle}>{child.title}</div>
            <div className={styles.contentBody}>{child.description}</div>
          </div>
          </Link>
        </div>
      ))}
    </Slider>
  );
};

export default EditorRecommendationCarousel;
