"use client";

import { PropsWithChildren, useRef } from "react";
import Slider from "react-slick";

import ArrowLeftSvg from "@/assets/svg/ArrowLeft";
import ArrowRightSvg from "@/assets/svg/ArrowRight";

interface SeriesCarouselProps extends PropsWithChildren {}

import styles from "./index.module.scss";

const SeriesCarousel = (props: SeriesCarouselProps) => {
  const { children, ...rest } = props;

  const sliderRef = useRef<any>(null);

  const DEFAULT_SETTINGS = {
    infinite: true,
    autoplay: true,
    pauseOnHover: true,
    swipeToSlide: true,
    slidesToShow: 5,
    slidesToScroll: 1,
    rows: 1,
    variableWidth: true,
  };

  return (
    <>
      <div className={styles.title}>
        <div className={styles.sectionHeading}>
          <div className={styles.rnLuynParent}>
            <b className={styles.rnLuyn}>Series</b>
            <div className={styles.rectangleWrapper}>
              <div className={styles.frameChild} />
            </div>
          </div>
        </div>
        <div className={styles.frameParent}>
          <div
            className={styles.arrowBackWrapper}
            onClick={() => sliderRef.current.slickPrev()}
          >
            <ArrowLeftSvg color="black" className={styles.arrowBackIcon1} />
          </div>
          <div
            className={styles.arrowBackWrapper}
            onClick={() => sliderRef.current.slickNext()}
          >
            <ArrowRightSvg color="black" className={styles.arrowBackIcon1} />
          </div>
        </div>
      </div>
      <Slider
        {...DEFAULT_SETTINGS}
        {...rest}
        className={styles.list}
        ref={sliderRef}
      >
        {children}
      </Slider>
    </>
  );
};

export default SeriesCarousel;
