import type { NextPage } from "next";
import size from "lodash/size";
import get from "lodash/get";
import { getSuggestionPosts } from "../../../api/post";
import EditorRecommendationCarousel from "../EditorRecommendationCarousel/index.client";

import styles from "./index.module.scss";

const EditorRecommendation: NextPage = async () => {
  const suggestionPosts = await getSuggestionPosts();
  const dataLength = size(suggestionPosts.data) > 0;

  return (
    dataLength &&
    <div className={styles.editorRecommendation}>
      <div className={styles.sectionHeading}>
        <div className={styles.headingTitle1}>Không thể bỏ lỡ</div>
        <div className={styles.main}>
          <b className={styles.headingTitle2}>Đề xuất từ</b>
          <div className={styles.line2}>
            <div className={styles.line2Inner}>
              <div className={styles.frameChild} />
            </div>
            <b className={styles.headingTitle2}>ban biên tập</b>
          </div>
        </div>
      </div>
      <div className={styles.card}>
        <EditorRecommendationCarousel data={get(suggestionPosts, 'data', [])} />
      </div>
    </div>
  );
};

export default EditorRecommendation;
