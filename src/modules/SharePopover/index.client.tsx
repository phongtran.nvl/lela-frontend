"use client";

import Image from "next/image";

import linkSvg from "@/assets/svg/link.svg";
import facebookSvg from "@/assets/svg/facebook.svg";
import twitterSvg from "@/assets/svg/negativeTwitter.svg";
import linkedInSvg from "@/assets/svg/negativeLinkedIn.svg";

import styles from "./index.module.scss";

interface SharePopoverProps {
  className?: string;
}

const SharePopover = (props: SharePopoverProps) => {
  const { className } = props;

  return (
    <div className={`${styles.popOverShare} ${className}`}>
      <div className={styles.linkParent}>
        <Image
          className={styles.linkedinNegative}
          alt="link svg"
          src={linkSvg}
        />
        <div className={styles.copyLink}>Copy link</div>
      </div>
      <div className={styles.facebookParent}>
        <Image
          className={styles.linkedinNegative}
          alt="facebook svg"
          src={facebookSvg}
        />
        <div className={styles.copyLink}>Share on Facebook</div>
      </div>
      <div className={styles.facebookParent}>
        <Image
          className={styles.linkedinNegative}
          alt="twitter svg"
          src={twitterSvg}
        />
        <div className={styles.copyLink}>Share on Twitter</div>
      </div>
      <div className={styles.facebookParent}>
        <Image
          className={styles.linkedinNegative}
          alt="linkedIn svg"
          src={linkedInSvg}
        />
        <div className={styles.copyLink}>Share on LinkedIn</div>
      </div>
    </div>
  );
};

export default SharePopover;
