"use client";
import { useState } from "react";
import Modal from "@mui/material/Modal";
import CloseSvg from "@/assets/svg/Close";

import styles from "./index.module.scss";
import './authModal.scss';
import SignInComponent from "@/modules/AuthModal/components/SignInComponent.client";
import SignUpComponent from "@/modules/AuthModal/components/SignUpComponent.client";

interface SignInModalProps {
  isOpen: boolean;
  handleClose: () => void;
}

enum Screen {
  "SIGN_IN",
  "SIGN_UP",
  "CONFIRMATION",
  "QUIZ_TOPICS",
  "QUIZ_INFO",
}

const SignInModal = (props: SignInModalProps) => {
  const { isOpen, handleClose } = props;
  const [page, setPage] = useState(Screen.SIGN_IN);

  const handleCloseClick = () => {
    handleClose();
    setPage(Screen.SIGN_IN);
  }

  const renderConfirmation = () => <>Confirmation</>;
  const renderContent = (
    <div className={styles.authWrapper}>
      {
        page === Screen.SIGN_IN && (
          <SignInComponent handleSwitchToSignUp={() => setPage(Screen.SIGN_UP)} />
        )
      }
      {
        page === Screen.SIGN_UP && (
          <SignUpComponent handleSwitchToLogin={() => setPage(Screen.SIGN_IN)} />
        )
      }
    </div>
  )


  return (
    <Modal
      open={isOpen}
      onClose={handleClose}
      aria-labelledby="modal-modal-title"
      aria-describedby="modal-modal-description"
    >
      <div className={styles.modal}>
        <CloseSvg
          color="#FFFFFF"
          className={styles.closeIcon1}
          onClick={handleCloseClick}
        />
        {renderContent}
      </div>
    </Modal>
  );
};

export default SignInModal;
