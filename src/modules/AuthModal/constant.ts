export const RELATIONSHIPS = [
	{
		value: "single",
		label: "Độc thân",
	},
	{
		value: "in_relationship",
		label: "Trong một mối quan hệ",
	},
	{
		value: "married",
		label: "Đã lập gia đình",
	},
	{
		value: "just_has_a_baby",
		label: "Vừa có con",
	},
]