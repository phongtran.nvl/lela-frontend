import Image from "next/image";
import UnreadEmail from "@/assets/svg/mail-unread.svg";
import styles from "@/modules/AuthModal/index.module.scss";

export default function ConfirmationComponent() {
	return <div className={styles.confirmationBox}>
		<Image src={UnreadEmail} alt="unread-email" className="m-auto" />
		<p className={styles.checkEmailText}>Kiểm tra email nhé!</p>
		<p className={styles.checkEmailSubText}>Link đăng nhập đã được gửi cho bạn rồi. Nếu bạn chưa nhận được trong 3 phút, kiểm tra thử hộp thư spam nhé.</p>
	</div>;
}