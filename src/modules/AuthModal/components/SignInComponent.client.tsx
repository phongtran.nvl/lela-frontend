import {Dispatch, SetStateAction, useState} from "react";
import styles from "@/modules/AuthModal/index.module.scss";
import {Button} from "@/components";
import Image from "next/image";
import vector3StrokeMiniSvg from "@/assets/svg/vector3StrokeMini.svg";
import originalGoogleSvg from "@/assets/svg/originalGoogle.svg";
import originalFacebookSvg from "@/assets/svg/originalFacebook.svg";
import clsx from "clsx";
import {TextField} from "@mui/material";
import {useForm} from "react-hook-form";
import * as yup from "yup";
import {yupResolver} from "@hookform/resolvers/yup";
import {loginByEmail} from "../../../../api/user";
import ConfirmationComponent from "@/modules/AuthModal/components/ConfirmComponent.client";
import {API_URL} from "@/utils";
import {useRouter} from "next/navigation";

enum SignInStep {
	SignIn = 0,
	Confirmation = 1
}

type Props = {
	handleSwitchToSignUp: Dispatch<SetStateAction<any>>
}
export default function SignInComponent({handleSwitchToSignUp}: Props) {
	const router = useRouter();
	const [step, setStep] = useState(SignInStep.SignIn);

	const registerSchema = yup.object().shape({
		email: yup.string().required('Vui lòng nhập email').email("Email không hợp lệ")
	});

	const {register, handleSubmit, formState: {errors}, getValues} = useForm({
		resolver: yupResolver(registerSchema)
	});

	const onSubmit = (data: any) => {
		loginByEmail(data.email).then(() => {
			setStep(SignInStep.Confirmation);
		});
	}

	const handleLoginFb = () => {
		router.push(API_URL + '/user/facebook');
	}

	const handleLoginGoogle = () => {
		router.push(API_URL + '/user/google');
	}

	const renderSignIn = (
		<>
			<b className={styles.ngNhp}>Đăng nhập</b>
			<div className={styles.form}>
				<div className={styles.inputWrapper}>
					<form onSubmit={handleSubmit(onSubmit)}>
						<div className={clsx(styles.inputElement, 'md:mb-7')}>
							<TextField
								placeholder="Email của tôi là..."
								className={styles.input}
								{...register("email")}
							/>
						</div>
						<div className={clsx(styles.primaryParent, 'md:mb-6')}>
							<Button
								className={clsx(styles.primary, 'md:mb-4')}
								mode="light"
								type="submit"
								disabled={!!errors.email || getValues('email') === ''}
							>
								<div className={styles.button}>Tiếp tục với email</div>
							</Button>
							<div className={styles.bnChaCContainer}>
								<span>Bạn chưa có tài khoản?</span>&nbsp;&nbsp;
								<span className='text-white cursor-pointer' onClick={handleSwitchToSignUp}>Đăng ký</span>
							</div>
						</div>
					</form>
				</div>
				<div className={clsx(styles.frameParent, 'md:mb-6')}>
					<div className={styles.rectangleWrapper}>
						<div className={styles.frameChild} />
					</div>
					<div className={styles.hoc}>hoặc</div>
					<Image
						className={styles.frameItem}
						alt="stroke line 3"
						src={vector3StrokeMiniSvg}
					/>
				</div>
				<div className={styles.secondaryParent}>
					<div className={clsx(styles.secondary, 'md:mb-4')} onClick={handleLoginGoogle}>
						<Image
							className={styles.iconLeft2}
							alt="google icon"
							src={originalGoogleSvg}
						/>
						<div className={styles.button1}>Tiếp tục với Google</div>
					</div>
					<div className={styles.secondary} onClick={handleLoginFb}>
						<Image
							className={styles.iconLeft2}
							alt="facebook icon"
							src={originalFacebookSvg}
						/>
						<div className={styles.button1}>Tiếp tục với Facebook</div>
					</div>
				</div>
			</div>
		</>
	)

	return (
		step === SignInStep.SignIn && renderSignIn ||
		step === SignInStep.Confirmation && <ConfirmationComponent />
	)
}