'use client';
import {Dispatch, memo, SetStateAction, useEffect, useState} from "react";
import styles from "@/modules/AuthModal/index.module.scss";
import {Button, Select} from "@/components";
import {FormControl, OutlinedInput, Select as MuiSelect} from '@mui/material';
import * as yup from 'yup';

import Image from "next/image";
import vector3StrokeSvg from "@/assets/svg/vector3Stroke.svg";
import {MenuItem, TextField} from "@mui/material";
import clsx from "clsx";
import vector3StrokeMiniSvg from "@/assets/svg/vector3StrokeMini.svg";
import originalGoogleSvg from "@/assets/svg/originalGoogle.svg";
import originalFacebookSvg from "@/assets/svg/originalFacebook.svg";
import {useForm} from "react-hook-form";
import {yupResolver} from '@hookform/resolvers/yup';
import get from "lodash/get";
import {CategoryType} from "../../../../api/types/category.type";
import {getCategories} from "../../../../api/category";
import isEmpty from "lodash/isEmpty";
import {RELATIONSHIPS} from "@/modules/AuthModal/constant";
import ConfirmationComponent from "@/modules/AuthModal/components/ConfirmComponent.client";
import {registerUser} from "../../../../api/user";
import {API_URL} from "@/utils";
import {useRouter} from "next/navigation";

type Props = {
	handleSwitchToLogin: Dispatch<SetStateAction<any>>;
}

type RegisterFirstStepDataType = {
	fullName: string;
	email: string;
}

type TotalRegisterDataType = {
	fullName: string;
	email: string;
	yearOfBirth?: number;
}

enum RegisterStep {
	FullNameAndEmail = 0,
	Quiz = 1,
	UserInfo = 2,
	Confirmation = 3
}

function SignUpComponent({handleSwitchToLogin}: Props) {
	const router = useRouter()
	const [topics, setTopics] = useState<Array<CategoryType>>([]);
	const [step, setStep] = useState(0);
	const [selectedTopic, setSelectedTopic] = useState<Array<string>>([]);
	const [relationship, setRelationship] = useState<string>("");
	const [registerData, setRegisterData] = useState<TotalRegisterDataType>({
		fullName: "phong",
		email: "p@gmail.com",
		yearOfBirth: 0,
	});

	useEffect(() => {
		(async () => {
			const topics = await getCategories();
			setTopics(get(topics, 'data', []));
		})()
	}, []);

	const registerSchema = yup.object().shape({
		fullName: yup.string().required("Vui lòng nhập họ tên"),
		email: yup.string().required('Vui lòng nhập email').email("Email không hợp lệ")
	});

	const {
		register,
		handleSubmit,
		formState: {errors},
	} = useForm<RegisterFirstStepDataType>({
		resolver: yupResolver(registerSchema),
		defaultValues: {
			fullName: registerData.fullName,
			email: registerData.email
		}
	});

	const onSubmit = (data: any) => {
		setRegisterData(data);
		setStep(RegisterStep.Quiz);
	}

	const handleLoginFb = () => {
		router.push(API_URL + '/user/facebook');
	}

	const handleLoginGoogle = () => {
		router.push(API_URL + '/user/google');
	}

	const onTopicChange = (value: string) => {
		const cloneSelectedTopic = [...selectedTopic];
		const index = cloneSelectedTopic.indexOf(value);
		if (index > -1) {
			cloneSelectedTopic.splice(index, 1);
		} else {
			cloneSelectedTopic.push(value);
		}
		setSelectedTopic([...cloneSelectedTopic]);
	}

	const onChangeYearOfBirth = (value: string) => {
		setRegisterData((prevState) => ({
			...prevState,
			yearOfBirth: Number(value)
		}))
	}

	const handleSetRelationship = (value: string) => {
		setRelationship(value);
	}

	const handleNextStepConfirm = () => {
		registerUser({
			email: registerData.email,
			fullName: registerData.fullName,
			categories: selectedTopic,
			userInformation: {
				yearOfBirth: registerData.yearOfBirth,
				relationship
			}
		}).then(() => {
			setStep(RegisterStep.Confirmation);
		})
	}

	const handleNextStepQuiz = () => {
		setStep(RegisterStep.UserInfo);
	}

	const renderSignUpStep = (
		<>
			<b className={styles.ngNhp}>Đăng ký</b>
			<div className={styles.inputWrapper}>
				<form onSubmit={handleSubmit(onSubmit)}>
					<div className={clsx(styles.inputElement, 'md:mb-4')}>
						<TextField
							placeholder="Tên tôi là..."
							className={styles.input}
							{...register("fullName")}
						/>
						{
							errors.fullName && <p className={styles.errorMessage}>{errors.fullName.message}</p>
						}
					</div>
					<div className={clsx(styles.inputElement, 'md:mb-7')}>
						<TextField
							placeholder="Email của tôi là..."
							className={styles.input}
							{...register("email")}
						/>
						{
							errors.email && <p className={styles.errorMessage}>{errors.email.message}</p>
						}
					</div>
					<div className={clsx(styles.primaryParent, 'md:mb-6')}>
						<Button
							className={clsx(styles.primary, 'md:mb-4')}
							mode="light"
							type="submit"
						>
							<div className={clsx(styles.button)}>Đăng ký</div>
						</Button>
						<div className={styles.bnChaCContainer}>
							<p className={clsx(styles.bnChaCTiKhon, 'text-sm')}>
								<span className='text-natural-600'>Bạn đã có tài khoản?</span>&nbsp;&nbsp;
								<span className='text-white cursor-pointer' onClick={handleSwitchToLogin}>Đăng nhập</span>
							</p>
						</div>
					</div>
				</form>
				<div className={clsx(styles.frameParent, 'md:mb-7')}>
					<div className={styles.rectangleWrapper}>
						<div className={styles.frameChild}/>
					</div>
					<div className={clsx(styles.hoc, 'md:mb-6')}>hoặc</div>
					<Image
						className={styles.frameItem}
						alt="stroke line 3"
						src={vector3StrokeMiniSvg}
					/>
				</div>
				<div className={styles.secondaryParent}>
					<div className={clsx(styles.secondary, 'md:mb-4')} onClick={handleLoginGoogle}>
						<Image
							className={styles.iconLeft2}
							alt="google icon"
							src={originalGoogleSvg}
						/>
						<div className={styles.socialButton}>Tiếp tục với Google</div>
					</div>
					<div className={styles.secondary} onClick={handleLoginFb}>
						<Image
							className={styles.iconLeft2}
							alt="facebook icon"
							src={originalFacebookSvg}
						/>
						<div className={styles.socialButton}>Tiếp tục với Facebook</div>
					</div>
				</div>
			</div>
		</>
	)

	const renderQuizTopics = (
		<>
			<div className={clsx(styles.headingTitleWrapper, 'md:mb-12')}>
				<div className={styles.topSubTitle}>Gợi ý bài viết</div>
				<b className={styles.suggestTopicText}>Theo chủ đề bạn yêu thích</b>
			</div>
			<div className={"w-full selectWrapper md:mb-12"}>
				{
					topics.map((topic: CategoryType) => {
						return <Select key={topic.id} label={topic.title} checked={selectedTopic.includes(topic.id)} onClick={() => onTopicChange(topic.id)}/>
					})
				}
			</div>
			<div className={styles.footerQuizWrapper}>
				<Image
					className={styles.frameItem}
					alt="stroke line 2"
					src={vector3StrokeSvg}
				/>
				<Button
					className={styles.quizContinueButton}
					onClick={handleNextStepQuiz}
					disabled={selectedTopic.length === 0}
				>
					<div className={styles.button}>Tiếp tục</div>
				</Button>
			</div>
		</>
	);

	const renderUserInformation = (
		<>
			<div className={clsx(styles.headingTitleWrapper, 'md:mb-10')}>
				<div className={styles.topSubTitle}>Gợi ý bài viết</div>
				<p className={styles.suggestPostByInfo}>Theo thông tin của bạn</p>
			</div>
			<div className={clsx(styles.inputsTextInputs, 'md:mb-6')}>
				<div className={clsx(styles.inputElement, 'w-full selectWrapper')}>
					<FormControl fullWidth>
						<MuiSelect
							displayEmpty
							className={styles.input}
							input={<OutlinedInput />}
							value={registerData.yearOfBirth || ''}
							onChange={(event) => onChangeYearOfBirth(String(event.target.value))}
						>
							<MenuItem disabled value="">
								<em>Chọn năm sinh</em>
							</MenuItem>
							{
								Array.from({ length: 100 }, (_, index) => 1923 + index).map((year) => (
									<MenuItem selected={year === registerData.yearOfBirth} key={year} value={year}>{year}</MenuItem>
								))
							}
						</MuiSelect>
					</FormControl>
				</div>
			</div>
			<div className={clsx(styles.selectSection, 'md:mb-8')}>
				<div className={styles.skipTextWrapper}>
					<div className={styles.skipText}>Bạn đang...</div>
				</div>
				<div className="w-full selectWrapper">
					{
						RELATIONSHIPS.map((item) => {
							return <Select key={item.value} label={item.label} checked={relationship === item.value} hasCheckbox={false} onClick={() => handleSetRelationship(item.value)}/>
						})
					}
				</div>
			</div>
			<div className={styles.footerQuizWrapper}>
				<Image
					className={styles.frameItem}
					alt="stroke line 2"
					src={vector3StrokeSvg}
				/>
				<div className={styles.buttonWrapper}>
					<Button
						className={styles.quizContinueButton}
						disabled={isEmpty(registerData.yearOfBirth) && isEmpty(relationship)}
						onClick={handleNextStepConfirm}
					>
						<div className={styles.button}>Tiếp tục</div>
					</Button>
					<div className={styles.skipTextWrapper} onClick={handleNextStepConfirm}>
						<div className={styles.skipText}>Bỏ qua</div>
					</div>
				</div>
			</div>
		</>
	);

	return (
		<div className={styles.signUpWrapper}>
			{
				step === RegisterStep.FullNameAndEmail && renderSignUpStep ||
				step === RegisterStep.Quiz && renderQuizTopics ||
				step === RegisterStep.UserInfo && renderUserInformation ||
				step === RegisterStep.Confirmation && <ConfirmationComponent />
			}
		</div>
	)
}

export default memo(SignUpComponent);