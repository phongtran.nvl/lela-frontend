import type { NextPage } from "next";
import Image from "next/image";

import defaultThumbnail from "../../../public/default-thumbnail.svg";

import ArrowRightSvg from "@/assets/svg/ArrowRight";

import styles from "./index.module.scss";
import { getLastestPostsFromRandomCategory } from "../../../api/post";
import get from "lodash/get";
import size from "lodash/size";
import map from "lodash/map";
import Link from "next/link";
import {getPostDetailPath} from "@/utils/path";

const CategoryLatestPosts: NextPage = async () => {
  const { data: posts } = await getLastestPostsFromRandomCategory();
  const firstPost = posts && posts[0];
  const restPosts = posts && posts.slice(1, 4);

  return (
    posts &&
    posts.length && (
      <div className={styles.categoryLatestPosts}>
        <div className={styles.sectionHeading}>
          <div className={styles.khngThB1}>Mới nhất trong</div>
          <div className={styles.rnLuynParent}>
            <b className={styles.rnLuyn1}>{get(firstPost, 'category.title', '')}</b>
            <div className={styles.rectangleWrapper}>
              <div className={styles.frameChild} />
            </div>
          </div>
        </div>
        <div className={styles.cardForHoverParent}>
          <div className={styles.cardForHover}>
            <Link href={getPostDetailPath(get(firstPost, 'slug', ''))}>
              <Image
                className={styles.imageIcon}
                alt="default image 1"
                src={get(firstPost, 'thumbnail', defaultThumbnail)}
                width={800}
                height={500}
              />
            </Link>
            <div className={styles.titleParent}>
              <div className={styles.title}>
                <Link href={getPostDetailPath(get(firstPost, 'slug', ''))}>
                  {get(firstPost, 'title', '')}
                </Link>
              </div>
              <div className={styles.tnhYuL1}>
                {get(firstPost, 'description', '')}
              </div>
              <div className={styles.biJasonLa5}>
                Bởi {get(firstPost, 'createdBy.fullName', '')}
              </div>
            </div>
          </div>
          <div className={styles.cardParent}>
            {size(restPosts) > 0  &&
              map(restPosts, ((post, index) => (
                <div className={styles.card} key={post.id}>
                  <Link href={getPostDetailPath(get(post, 'slug', ''))}>
                    <Image
                      className={styles.image91Icon}
                      alt={`default image ${++index}`}
                      src={post.thumbnail ? post.thumbnail : defaultThumbnail}
                      width={120}
                      height={120}
                    />
                  </Link>
                  <div className={styles.content}>
                    <div className={styles.tKSContainer}>
                      <Link href={getPostDetailPath(get(post, 'slug', ''))}>
                        {post.title}
                      </Link>
                    </div>
                    <div className={styles.biJasonLa6}>
                      Bởi {post.createdBy.fullName}
                    </div>
                  </div>
                </div>
              )))}
          </div>
          <div className={styles.ghostMobile}>
            <div className={styles.iconLeft1}>
              <ArrowRightSvg color="#FFFFFF" className={styles.iconLeft2} />
            </div>
            <div className={styles.button}>Xem tất cả</div>
          </div>
        </div>
      </div>
    )
  );
};

export default CategoryLatestPosts;
