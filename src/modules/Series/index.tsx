import Image from "next/image";

import SeriesCarousel from "../SeriesCarousel/index.client";
import ArrowRightSvg from "@/assets/svg/ArrowRight";

import defaultFrame1 from "@/assets/png/default/series/defaultFrame1.png";
import defaultFrame2 from "@/assets/png/default/series/defaultFrame2.png";
import defaultFrame3 from "@/assets/png/default/series/defaultFrame3.png";
import defaultFrame4 from "@/assets/png/default/series/defaultFrame4.png";
import defaultFrame5 from "@/assets/png/default/series/defaultFrame5.png";
import defaultFrame6 from "@/assets/png/default/series/defaultFrame6.png";
import defaultFrame7 from "@/assets/png/default/series/defaultFrame7.png";

import styles from "./index.module.scss";

const CONTENT = (
  <>
    <div className={styles.card}>
      <div className={styles.frameGroup}>
        <Image
          className={styles.frameItem}
          alt="default frame 1"
          src={defaultFrame1}
        />
        <b className={styles.chnI}>Chốn đi</b>
      </div>
      <div className={styles.ghost}>
        <div className={styles.iconLeft7}>
          <ArrowRightSvg color="#143080" className={styles.iconLeft8} />
        </div>
        <div className={styles.button}>Xem 154 bài</div>
      </div>
    </div>
    <div className={styles.card1}>
      <div className={styles.frameGroup}>
        <Image
          className={styles.frameItem}
          alt="default frame 2"
          src={defaultFrame2}
        />
        <b className={styles.chnI}>Chuyện nhà</b>
      </div>
      <div className={styles.ghost1}>
        <div className={styles.iconLeft9}>
          <ArrowRightSvg color="#FFFFFF" className={styles.iconLeft8} />
        </div>
        <div className={styles.button}>Xem 154 bài</div>
      </div>
    </div>
    <div className={styles.card}>
      <div className={styles.frameGroup}>
        <Image
          className={styles.frameItem}
          alt="default frame 3"
          src={defaultFrame3}
        />{" "}
        <b className={styles.chnI}>LeLa News</b>
      </div>
      <div className={styles.ghost}>
        <div className={styles.iconLeft7}>
          <ArrowRightSvg color="#143080" className={styles.iconLeft8} />
        </div>
        <div className={styles.button}>Xem 154 bài</div>
      </div>
    </div>
    <div className={styles.card1}>
      <div className={styles.frameParent2}>
        <Image
          className={styles.frameItem}
          alt="default frame 4"
          src={defaultFrame4}
        />{" "}
        <b className={styles.chnI}>Sự kiện</b>
      </div>
      <div className={styles.ghost1}>
        <div className={styles.iconLeft9}>
          <ArrowRightSvg color="#FFFFFF" className={styles.iconLeft8} />
        </div>
        <div className={styles.button}>Xem 154 bài</div>
      </div>
    </div>
    <div className={styles.card4}>
      <div className={styles.frameGroup}>
        <div className={styles.groupWrapper}>
          <div className={styles.vectorParent}>
            <Image
              className={styles.frameItem}
              alt="default frame 5"
              src={defaultFrame5}
            />
          </div>
        </div>
        <b className={styles.viThHay1}>Vài thứ hay ho</b>
      </div>
      <div className={styles.ghost4}>
        <div className={styles.iconLeft7}>
          <ArrowRightSvg color="#143080" className={styles.iconLeft8} />
        </div>
        <div className={styles.button}>Xem 154 bài</div>
      </div>
    </div>
    <div className={styles.card5}>
      <div className={styles.frameParent2}>
        <div className={styles.frameItem}>
          <Image
            className={styles.frameItem}
            alt="default frame 6"
            src={defaultFrame6}
          />
          <div className={styles.l}>L</div>
        </div>
        <b className={styles.lelaShopping}>LeLa Shopping</b>
      </div>
      <div className={styles.ghost5}>
        <div className={styles.iconLeft9}>
          <ArrowRightSvg color="#FFFFFF" className={styles.iconLeft8} />
        </div>
        <div className={styles.button}>Xem 154 bài</div>
      </div>
    </div>
    <div className={styles.card6}>
      <div className={styles.frameParent2}>
        <div className={styles.frameItem}>
          <Image
            className={styles.frameItem}
            alt="default frame 7"
            src={defaultFrame7}
          />
        </div>
        <b className={styles.viThHay1}>Cưng muốn xỉu</b>
      </div>
      <div className={styles.ghost}>
        <div className={styles.iconLeft7}>
          <ArrowRightSvg color="#143080" className={styles.iconLeft8} />
        </div>
        <div className={styles.button}>Xem 154 bài</div>
      </div>
    </div>
  </>
);

const Series = () => {
  return (
    <div className={styles.series}>
      <SeriesCarousel>{CONTENT}</SeriesCarousel>
    </div>
  );
};

export default Series;
