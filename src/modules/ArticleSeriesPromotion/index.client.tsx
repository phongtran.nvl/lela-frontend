import Image from "next/image";

import { TextDisplay, Button } from "@/components";

import vector2Svg from "@/assets/svg/vector2.svg";

import styles from "./index.module.scss";

interface ArticleSeriesPromotionProps {
  className?: string;
}

const ArticleSeriesPromotion = (props: ArticleSeriesPromotionProps) => {
  const { className } = props;

  return (
    <div className={`${styles.articleSeriesPromotion} ${className}`}>
      <div className={styles.content}>
        <TextDisplay className={styles.bnThyBi1}>
          Bạn thấy bài viết thú vị và muốn đọc thêm về LeLa News?
        </TextDisplay>
        <TextDisplay className={styles.lelaNewsL}>
          LeLa News là series nói về kinh doanh, khởi nghiệp, kinh nghiệm sống
          và những cuộc trò chuyện truyền cảm hứng.
        </TextDisplay>
        <div className={styles.primaryParent}>
          <Button className={styles.primary}>
            <div className={styles.button}>Khám phá thêm</div>
          </Button>
          <Button className={styles.secondary} button="secondary">
            <div className={styles.button}>Theo dõi ngay</div>
          </Button>
        </div>
      </div>
      <Image alt="vector 2" src={vector2Svg} />
    </div>
  );
};

export default ArticleSeriesPromotion;
