"use client";

import { TextInput, Button } from "@/components";

import styles from "./index.module.scss";
import {useEffect, useState} from "react";

interface SubscribeBoxProps {
  className?: string;
}

const SubscribeBox = (props: SubscribeBoxProps) => {
  const [contentReady, setContentReady] = useState<boolean>(false);
  const { className } = props;

  useEffect(() => {
    setContentReady(true);
  }, []);

  if (!contentReady) return null;

  return (
    <div className={styles.subscribeBox}>
      <div className={styles.graphicKvParent}>
        <div className={`${styles.ngK} ${className}`}>
          <div className={styles.text}>
            <div className={styles.ngKNhn1}>Đăng ký nhận tin</div>
            <b className={styles.nhngBiVit}>
              Nếu bạn không muốn bỏ lỡ những bài viết thú vị
            </b>
          </div>
          <div className={styles.form}>
            <div className={styles.inputstextInputs}>
              <div className={styles.type}>
                <TextInput
                  className={styles.linLcVi1}
                  placeholder="Email của tôi là..."
                />
              </div>
            </div>
            <Button className={styles.primary}>
              <div className={styles.button}>Đăng ký</div>
            </Button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default SubscribeBox;
