'use client';
import {useEffect, useState} from "react";
import Image from "next/image";
import defaultImage from "@/assets/png/default/categoryLatestPosts/defaultImage1.png";
import styles from "./index.module.scss";

const ArticleRecentlyViewed = () => {
  const [contentReady, setContentReady] = useState<boolean>(false);

  useEffect(() => {
    setContentReady(true);
  }, []);

  if (!contentReady) return null;

  return (
    <div className={styles.articleRecentlyViewed}>
      <div className={styles.titleWrapper}>
        <div className={styles.title}>Mọi người đều đọc</div>
      </div>
      <div className={styles.card}>
        <Image className={styles.image91Icon} alt="image" src={defaultImage} />
        <div className={styles.content}>
          <div className={styles.link}>
            <div className={styles.link1}>Dinh dưỡng</div>
          </div>
          <div className={styles.subTitle}>
            Tempeh: Thực phẩm hoàn hảo cho người ăn thuần chay
          </div>
          <div className={styles.writtenBy}>{`Bởi Jason La `}</div>
        </div>
      </div>
      <div className={styles.card3}>
        <Image className={styles.image91Icon} alt="" src={defaultImage} />
        <div className={styles.content}>
          <div className={styles.link}>
            <div className={styles.link1}>Rèn luyện</div>
          </div>
          <div className={styles.subTitle}>
            Tập luyện tại nhà, cải thiện vòng 3
          </div>
          <div className={styles.writtenBy}>{`Bởi Jason La `}</div>
        </div>
      </div>
    </div>
  );
};

export default ArticleRecentlyViewed;
