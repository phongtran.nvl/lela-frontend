"use client";

import { ShortQuote, LongQuote } from "@/components";

interface ArticleSpecialSectionProps {
  shortQuote?: string;
  longQuote?: string;
  poll?: string;
  readMore?: string;
}

const ArticleSpecialSection = (props: ArticleSpecialSectionProps) => {
  const { shortQuote, poll, longQuote, readMore } = props;

  const renderShortQuote = () => <ShortQuote content={shortQuote} />;
  const renderLongQuote = () => <LongQuote content={longQuote} />;

  return (
    <>
      {shortQuote && renderShortQuote()}
      {longQuote && renderLongQuote()}
    </>
  );
};

export default ArticleSpecialSection;
