import Image from "next/image";

import HighlightCarousel from "../HighlightCarousel/index.client";

import default1 from "@/assets/png/default/highlight/highlightDefault.png";
import default2 from "@/assets/png/default/highlight/highlightDefault1.png";
import default3 from "@/assets/png/default/highlight/highlightDefault2.png";
import default4 from "@/assets/png/default/highlight/highlightDefault3.png";
import default5 from "@/assets/png/default/highlight/highlightDefault4.png";
import default6 from "@/assets/png/default/highlight/highlightDefault5.png";
import default7 from "@/assets/png/default/highlight/highlightDefault6.png";

import styles from "./index.module.scss";

const CONTENT = (
  <>
    <div className={styles.card} style={{ width: "288px" }}>
      <Image
        className={styles.imageIcon}
        alt="default image 1"
        src={default1}
      />
      <div className={styles.content}>
        <div className={styles.link}>
          <div className={styles.link1}>Ăn gì, ở đâu với ai</div>
        </div>
        <div className={styles.title}>Ăn mì Ý, uống rượu gì?</div>
        <div className={styles.writtenBy}>Bởi Jason La</div>
      </div>
    </div>
    <div className={styles.card} style={{ width: "288px" }}>
      <Image
        className={styles.imageIcon1}
        alt="default image 2"
        src={default2}
      />
      <div className={styles.content}>
        <div className={styles.link}>
          <div className={styles.link1}>Rèn luyện</div>
        </div>
        <div className={styles.title}>
          Những bài tập Yoga giúp bạn dễ chịu hơn trong kỳ kinh nguyệt
        </div>
        <div className={styles.writtenBy}>{`Bởi Jason La `}</div>
      </div>
    </div>
    <div className={styles.card} style={{ width: "600px" }}>
      <Image
        className={styles.imageIcon2}
        alt="default image 3"
        src={default3}
      />
      <div className={styles.content}>
        <div className={styles.link}>
          <div className={styles.link1}>LeLa News</div>
        </div>
        <div className={styles.title2}>
          Nhiếp ảnh gia Lê Hồng Linh: &quot;Trước khi sống vì mình, hãy học cách
          sống cho người khác&quot;
        </div>
        <div className={styles.writtenBy}>{`Bởi Jason La `}</div>
      </div>
    </div>
    <div className={styles.card} style={{ width: "288px" }}>
      <Image
        className={styles.imageIcon}
        alt="default image 4"
        src={default4}
      />
      <div className={styles.content}>
        <div className={styles.link}>
          <div className={styles.link1}>LeLa News</div>
        </div>
        <div className={styles.title}>
          Đạo diễn Trịnh Đình Lê Minh: Điện ảnh không có lằn ranh giữa
          &quot;tôi&quot; và &quot;chúng ta&quot;
        </div>
        <div className={styles.writtenBy}>{`Bởi Jason La `}</div>
      </div>
    </div>
    <div className={styles.card} style={{ width: "288px" }}>
      <Image
        className={styles.imageIcon1}
        alt="default image 5"
        src={default5}
      />
      <div className={styles.content}>
        <div className={styles.link}>
          <div className={styles.link1}>Thú cưng</div>
        </div>
        <div className={styles.title}>
          Muốn chó ngừng sủa? Áp dụng ngay 2 bước đơn giản
        </div>
        <div className={styles.writtenBy}>{`Bởi Jason La `}</div>
      </div>
    </div>
    <div className={styles.card} style={{ width: "600px" }}>
      <Image
        className={styles.imageIcon2}
        alt="default image 6"
        src={default6}
      />
      <div className={styles.content}>
        <div className={styles.link}>
          <div className={styles.link1}>Tâm lý</div>
        </div>
        <div className={styles.title2}>
          Mâu thuẫn giữa anh chị em: Làm sao khi &quot;gà cùng một mẹ&quot; cứ
          hoài đá nhau?
        </div>
        <div className={styles.writtenBy}>{`Bởi Jason La `}</div>
      </div>
    </div>
    <div className={styles.card} style={{ width: "288px" }}>
      <Image
        className={styles.imageIcon}
        alt="default image 7"
        src={default7}
      />
      <div className={styles.content}>
        <div className={styles.link}>
          <div className={styles.link1}>Dinh dưỡng</div>
        </div>
        <div className={styles.title}>
          Thức ăn nào giúp cơ thể có nhiều năng lượng hơn?
        </div>
        <div className={styles.writtenBy}>{`Bởi Jason La `}</div>
      </div>
    </div>
  </>
);

const Highlight = async () => {
  return (
    <div className={styles.highlight}>
      <div className={styles.frameParent}>
        <HighlightCarousel>{CONTENT}</HighlightCarousel>
      </div>
    </div>
  );
};

export default Highlight;
