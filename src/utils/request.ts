import axios, {AxiosResponse} from 'axios';
import {camelizeKeys, decamelizeKeys} from 'humps';
import get from "lodash/get";

export type ErrorResponseType = {
	error: boolean;
	message: string;
	statusCode: number;
	data?: [] | null;
}

export const fetcher = axios.create({
	baseURL: process.env.NEXT_PUBLIC_API_URI,
});

fetcher.interceptors.response.use((response: AxiosResponse) => {
	response.data = camelizeKeys(response.data);
	return response.data
}, (error) => {
	const errorResponse: ErrorResponseType = {
		error: true,
		message: get(error, 'response.data.message', error.message),
		statusCode: get(error, 'response.status', error.status),
		data: get(error, 'response.data.data', [])
	}
	return new Promise((resolve, reject) => {
		reject(errorResponse);
	});
});

fetcher.interceptors.request.use((config) => {
	const request = config;
	if (request?.data) {
		request.data = decamelizeKeys(request.data);
	}
	if (request?.params) {
		request.params = decamelizeKeys(request.params);
	}
	return request;
})


// export const fetcher = async (url: string, options: RequestInit = {}) => {
// 	const result = await fetch(`${process.env.API_URI}/${url}`, options);
// 	if (result.ok) {
// 		return camelizeKeys(await result.json());
// 	}
// 	return {
// 		error: true,
// 		message: result.statusText,
// 		statusCode: result.status
// 	}
// }