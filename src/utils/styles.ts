export const getSizeStyle = (parentClass: string, size?: string) => {
  let className = "";

  switch (size) {
    case "xs":
      className = `${parentClass}__xs`;
      break;
    case "s":
      className = `${parentClass}__s`;
      break;
    case "m":
      className = `${parentClass}__m`;
      break;
    default:
      className = `${parentClass}__s`;
      break;
  }

  return className;
};
