import find from 'lodash/find';

type Setting = {
	key: string;
	value: string;
	name: string;
}
export default function getSettingValue (props: Setting[], key: string): string {
	const findSetting = find(props, { key });
	if (findSetting) {
		return findSetting.value;
	}
	return "";
}