const paths = {
	home: '/',
	postDetail: '/post/:slug'
}
export default paths

export const getPostDetailPath = (slug: string) => {
	return paths.postDetail.replace(':slug', slug)
}