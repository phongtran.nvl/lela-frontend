"use client";

import styles from "./index.module.scss";

interface LongQuoteProps {
  content?: string;
}

const LongQuote = (props: LongQuoteProps) => {
  const { content } = props;

  return (
    <div className={styles.longQuote}>
      <div className={styles.leftHorizontalLine} />
      <div className={styles.longQuoteContent}>{content}</div>
    </div>
  );
};

export default LongQuote;
