"use client";

import {useEffect, useState} from "react";
import Image from "next/image";
import Link from "next/link";

import { Button, TextInput } from "@/components";

import leLaSvg from "@/assets/svg/leLa.svg";
import searchSvg from "@/assets/svg/search.svg";
import chevronDown from "@/assets/svg/chevronDown.svg";
import menu from "@/assets/svg/menu.svg";

import styles from "./index.module.scss";
import SignInModal from "@/modules/AuthModal/index.client";
import {useSearchParams} from "next/navigation";
import {getAccessToken, getUserProfile, loginByToken, removeAccessToken, storageAccessToken} from "../../../api/user";
import {useUser} from "@/hook/useUser";
import isEmpty from "lodash/isEmpty";
import get from "lodash/get";

const StickyNavbar = () => {
  const params = useSearchParams();
  const {user, setUser} = useUser();

  useEffect(() => {
    if (params.has("token")) {
      const token = params.get("token");
      if (token) {
        loginByToken(token).then((res) => {
          storageAccessToken(res.data.accessToken);
          setTimeout(() => {
            window.location.href = `${window.location.origin}${window.location.pathname}`
          }, 500)
        });
      }
    }
  }, [params]);

  useEffect(() => {
    const token = getAccessToken();
    if (token && isEmpty(user.fullName)) {
      getUserProfile(token).then((res) => {
        setUser(res.data);
      }).catch(() => {
        removeAccessToken();
        setTimeout(() => {
          window.location.reload();
        }, 500);
      })
    }
  }, [setUser, user]);

  const [isOpen, setIsOpen] = useState(false);

  return (
    <nav className={styles.stickyNavbar}>
      <div className={styles.frameParent}>
        <div className={styles.lelaWrapper}>
          <Link href="/">
            <Image className={styles.lelaIcon} alt="Le La" src={leLaSvg} />
          </Link>
        </div>
        <div className={styles.inputsTextInputs}>
          <div className={styles.type}>
            <Image
              className={styles.iconLeft2}
              alt="search icon"
              src={searchSvg}
            />
            {/* <div className={styles.inputWrapper}>
              <input placeholder="Tìm chủ đề, series, tag, tác giả..." />
            </div> */}
            <TextInput
              placeholder="Tìm chủ đề, series, tag, tác giả..."
              className={styles.inputWrapper}
            />
          </div>
        </div>
        <div className={styles.ghost}>
          <div className={styles.iconLeft3}>
            <Image
              className={styles.iconLeft4}
              alt="chevron down icon"
              src={chevronDown}
            />
          </div>
          <div className={styles.button}>Series</div>
          <Image src={menu} alt="menu" className={styles.menu} />
        </div>
      </div>
      <div className={styles.stateDefaultChild} />
      <div className={styles.ghostParent}>
        <div className={styles.ghost1}>
          <div className={styles.button}>Tham gia viết bài</div>
        </div>
        {
          !get(user, 'fullName', null) ? (
            <Button className={styles.primary} onClick={() => setIsOpen(true)}>
              <div className={styles.button}>Đăng nhập</div>
            </Button>
          ) : <span>{user.fullName}</span>
        }
      </div>
      <SignInModal isOpen={isOpen} handleClose={() => setIsOpen(false)} />
    </nav>
  );
};

export default StickyNavbar;
