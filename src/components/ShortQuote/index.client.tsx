"use client";

import { useState } from "react";
import Button from "@/components/Button/index.client";
import SharePopover from "@/modules/SharePopover/index.client";

import styles from "./index.module.scss";

interface ShortQuoteProps {
  content?: string;
}

const ShortQuote = (props: ShortQuoteProps) => {
  const [popoverOpen, setPopoverOpen] = useState<boolean>(false);

  const { content } = props;

  return (
    <div className={styles.shortQuote}>
      <div className={styles.shortQuoteContent}>{content}</div>
      <Button
        className={styles.secondary}
        button="secondary"
        onClick={() => setPopoverOpen(!popoverOpen)}
      >
        <div className={styles.button}>Chia sẻ</div>
        {popoverOpen && <SharePopover className={styles.sharePopoverWrapper} />}
      </Button>
    </div>
  );
};

export default ShortQuote;
