import Input, { InputProps } from "@mui/material/Input";
import styles from "./index.module.scss";

interface TextInputProps extends InputProps {
  label?: string;
  wrapperClassName?: string;
}

const TextInput = (props: TextInputProps): JSX.Element => {
  const { className, wrapperClassName, label, ...rest } = props;

  return (
    <div className={`${styles.defaultInputWrapper} ${wrapperClassName}`}>
      {label && <label className={styles.email}>{label}</label>}
      <Input
        {...rest}
        disableUnderline={true}
        className={`${styles.defaultInput} ${className}`}
      />
    </div>
  );
};

export default TextInput;
