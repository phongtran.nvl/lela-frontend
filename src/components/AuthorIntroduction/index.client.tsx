'use client'
import {useEffect, useState} from "react";
import Image, { StaticImageData } from "next/image";

import Icon from "../Icon/index.client";

import authorDefaultAvatar from "@/assets/png/default/article/authorDefaultAvatar.png";
import arrowRightSvg from "@/assets/svg/arrowRight.svg";

import styles from "./index.module.scss";

interface AuthorIntroductionProps {
  avtSrc?: StaticImageData | string;
  name: string;
  brief: string;
  className?: string;
}

const AuthorIntroduction = (props: AuthorIntroductionProps) => {
  const [contentReady, setContentReady] = useState<boolean>(false);
  const { avtSrc , name, brief, className } = props;

  useEffect(() => {
    setContentReady(true);
  }, []);

  if (!contentReady) return null;
  return (
    <div className={`${styles.authorIntroduction} ${className}`}>
      <Image className={styles.authorAvatar} alt="author avatar" src={avtSrc || authorDefaultAvatar} width="64" height={64} />
      <div className={styles.authorBody}>
        <div className={styles.authorNameAndBrief}>
          <div className={styles.authorName}>{name}</div>
          <div className={styles.authorBrief}>{brief}</div>
        </div>
        <div className={styles.ghost}>
          <Icon
            className={styles.iconLeft1}
            iconClassName={styles.iconLeft2}
            alt="arrow right"
            src={arrowRightSvg}
          />
          <div className={styles.button}>Xem profile</div>
        </div>
      </div>
    </div>
  );
};

export default AuthorIntroduction;
