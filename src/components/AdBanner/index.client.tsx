"use client";

import React from "react";
import Image, { StaticImageData } from "next/image";

import ad_banner from "@/assets/png/ad_banner.png";
import styles from "./index.module.scss";

interface AdBannerProps {
  imgSrc?: StaticImageData | string;
  type?: "internal" | "external";
}
const AdBanner = (props: AdBannerProps): JSX.Element => {
  const { imgSrc, type = "internal" } = props;

  return (
    <div className={styles.adBanner}>
      <div
        className={`${styles.image141Parent} ${
          type === "internal" ? styles.internalBanner : styles.externalBanner
        }`}
      >
        <Image
          className={styles.image141Icon}
          alt=""
          src={imgSrc || ad_banner}
        />
      </div>
    </div>
  );
};

export default AdBanner;
