"use client";

import { useState, useEffect } from "react";
import { Icon } from "@/components";

import arrowUpSvg from "@/assets/svg/arrowUp.svg";
import styles from "./index.module.scss";

function scrollToTop() {
  window.scrollTo({ top: 0, behavior: "smooth" });
}

export default function ScrollToTop() {
  const [isVisible, setIsVisible] = useState(false);

  useEffect(() => {
    // Define a function that is called when the scroll event fires
    const handleScroll = (e: any) => {
      const scrollTop = e.target.documentElement.scrollTop;

      if (scrollTop > 500) {
        setIsVisible(true);
      } else {
        setIsVisible(false);
      }
    };

    // Add the event listener inside a useEffect
    if (document) {
      document.addEventListener("scroll", handleScroll);
    }

    // Remove the event listener on unmount
    return () => {
      if (document) {
        document.removeEventListener("scroll", handleScroll);
      }
    };
  }, [setIsVisible]);

  return (
    isVisible && (
      <div className={styles.icon}>
        <Icon
          iconClassName={styles.arrowUpIcon1}
          alt="arrow up"
          src={arrowUpSvg}
          size="m"
          onClick={scrollToTop}
        />
      </div>
    )
  );
}
