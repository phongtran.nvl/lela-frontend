'use client'
import {useEffect, useState} from "react";
import styles from "./index.module.scss";


interface Tag {
  title: string;
  slug: string;
}
interface TagsProps {
  tags?: Array<Tag>;
  className?: string;
}

const Tags = (props: TagsProps) => {
  const [contentReady, setContentReady] = useState<boolean>(false);
  const { className, tags = [] } = props;

  useEffect(() => {
    setContentReady(true);
  }, []);

  if (!contentReady) return null;

  return (
    <div className={`${styles.tags} ${className}`}>
      {tags.length >= 1 &&
        tags.map((item) => (
          <div key={item.title} className={`${styles.tag}`}>
            {item.title}
          </div>
        ))}
    </div>
  );
};

export default Tags;
