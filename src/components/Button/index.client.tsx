"use client";

import React from "react";
import { default as MuiButton, ButtonProps } from "@mui/material/Button";

interface MyButtonProps extends ButtonProps {
  mode?: "light" | "dark";
  btnSize?: "small" | "medium";
  button?: "primary" | "secondary";
}

import styles from "./index.module.scss";

const Button = (props: MyButtonProps) => {
  const {
    children,
    className,
    variant,
    mode,
    btnSize,
    disabled,
    button,
    ...rest
  } = props;

  return (
    <MuiButton
      {...rest}
      className={`${styles.myButton} ${btnSize ? styles[btnSize] : ""}
      ${mode ? styles[mode] : ""}
      ${button ? styles[button] : ""}
       ${className}`}
      variant={variant ?? "contained"}
      disabled={disabled}
    >
      {children}
    </MuiButton>
  );
};

export default Button;
