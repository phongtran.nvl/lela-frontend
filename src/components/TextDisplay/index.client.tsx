import { PropsWithChildren } from "react";

import Typography from "@mui/material/Typography";

interface TextDisplayProps extends PropsWithChildren {
  content?: string;
  className?: string;
}

const TextDisplay = (props: TextDisplayProps) => {
  const { content, className, children } = props;

  return (
    <div className={`text-display ${className}`}>{children || content}</div>
  );
};

export default TextDisplay;
