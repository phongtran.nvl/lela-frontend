import type { NextPage } from "next";
import Image from "next/image";

import TextInput from "../TextInput/index.client";
import Button from "../Button/index.client";

import leLaSvg from "@/assets/svg/leLa.svg";
import facebookSvg from "@/assets/svg/transparentFacebook.svg";
import instagramSvg from "@/assets/svg/transparentInstagram.svg";
import linkedInSvg from "@/assets/svg/transparentLinkedin.svg";
import tikTokSvg from "@/assets/svg/negativeTikTok.svg";
import youtubeSvg from "@/assets/svg/negativeYouTube.svg";

import styles from "./index.module.scss";

const Footer: NextPage = () => {
  return (
    <div className={styles.footer}>
      <div className={styles.ngK}>
        <Image className={styles.lelaIcon} alt="Le La" src={leLaSvg} />
        <div className={styles.frameParent}>
          <div className={styles.frameGroup}>
            <div className={styles.ngKNhnTinParent}>
              <div className={styles.ngKNhn1}>Đăng ký nhận tin</div>
              <div className={styles.nuBnKhng1}>
                Nếu bạn không muốn bỏ lỡ những bài viết thú vị
              </div>
            </div>
            <div className={styles.inputstextInputs}>
              <div className={styles.type}>
                <TextInput
                  placeholder="Email của tôi là..."
                  className={styles.linLcVi1}
                />
              </div>
            </div>
          </div>
          <Button className={styles.primary} mode="light">
            <div className={styles.button}>Đăng ký</div>
          </Button>
        </div>
      </div>
      <div className={styles.frameDiv}>
        <div className={styles.frameParent1}>
          <div className={styles.seriesParent}>
            <div className={styles.series}>Series</div>
            <div className={styles.frameParent2}>
              <div className={styles.viThHayHoParent}>
                <div className={styles.button}>Vài thứ hay ho</div>
                <div className={styles.button}>Chốn đi</div>
                <div className={styles.button}>Chốn về</div>
                <div className={styles.button}>Cưng muốn xỉu</div>
              </div>
              <div className={styles.viThHayHoParent}>
                <div className={styles.button}>Sự kiện</div>
                <div className={styles.button}>LeLa News</div>
                <div className={styles.button}>LeLa Shopping</div>
              </div>
            </div>
          </div>
          <div className={styles.lelaJournalParent}>
            <div className={styles.series}>LeLa Journal</div>
            <div className={styles.vLelaParent}>
              <div className={styles.vLela}>Về LeLa</div>
              <div className={styles.vLela}>Tham gia viết bài</div>
              <div className={styles.vLela}>Liên hệ</div>
            </div>
          </div>
        </div>
        <div className={styles.logoFacebookParent}>
          <Image
            className={styles.tiktokNegative}
            alt="facebook"
            src={facebookSvg}
          />
          <Image
            className={styles.tiktokNegative}
            alt="instagram"
            src={instagramSvg}
          />
          <Image
            className={styles.tiktokNegative}
            alt="linkedIn"
            src={linkedInSvg}
          />
          <Image
            className={styles.whiteNegative}
            alt="tikTok"
            src={tikTokSvg}
          />
          <Image
            className={styles.whiteNegative}
            alt="youtube"
            src={youtubeSvg}
          />
        </div>
        <div className={styles.byLelaJournalLimitedAllRParent}>
          <div className={styles.series}>
            ©2023 by LeLa Journal Limited. All Rights Reserved.
          </div>
          <div className={styles.chnhSchBoMtParent}>
            <div className={styles.button}>Chính sách bảo mật</div>
            <div className={styles.frameChild} />
            <div className={styles.button}>Thỏa thuận người dùng</div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Footer;
