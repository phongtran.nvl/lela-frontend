import { ComponentProps } from "react";
import Image, { StaticImageData } from "next/image";

import { getSizeStyle } from "@/utils";

import styles from "./index.module.scss";

interface IconProps extends ComponentProps<"div"> {
  src: string | StaticImageData;
  alt?: string;
  size?: "xs" | "s" | "m";
  iconClassName?: string;
}

const Icon = (props: IconProps) => {
  const { src, className, alt, size, iconClassName, onClick } = props;

  return (
    <div
      className={`${styles.defaultIconWrapper} ${
        styles[getSizeStyle("defaultIconWrapper", size)]
      }
      ${className}`}
      onClick={onClick}
    >
      <Image
        src={src}
        alt={alt || "icon"}
        className={`${styles.defaultIcon} ${
          styles[getSizeStyle("defaultIcon", size)]
        } ${iconClassName}`}
      />
    </div>
  );
};

export default Icon;
