"use client";

import {memo, useState} from "react";
import FormControlLabel from "@mui/material/FormControlLabel";
import Checkbox, { CheckboxProps } from "@mui/material/Checkbox";

import styles from "./index.module.scss";
import clsx from "clsx";

interface SelectProps {
  checked?: boolean;
  defaultChecked?: boolean;
  label?: string;
  hasCheckbox?: boolean;
  onClick: Function;
}

const Select = (props: SelectProps) => {
  const { label, defaultChecked, checked, hasCheckbox = true, onClick, ...rest } = props;

  return (
    <FormControlLabel
      {...rest}
      checked={checked}
      onClick={() => {
        if (!hasCheckbox) {
          onClick();
        }
      }}
      control={
        hasCheckbox ? (
          <Checkbox
            onClick={() => {
              onClick();
            }}
            defaultChecked={defaultChecked}
            checked={checked}
            className={styles.selectCheckbox}
            sx={{
              color: checked ? "#143080" : "#FFF",
              "&.Mui-checked": {
                color: "#143080",
              },
            }}
          />
        ) : (
          <></>
        )
      }
      label={label}
      className={clsx(styles.selectWrapper, {[styles.selectWrapperChecked]: checked})}
    />
  );
};

export default memo(Select);
