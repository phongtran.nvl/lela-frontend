import Image, { StaticImageData } from "next/image";

import styles from "./index.module.scss";

interface AvatarProps {
  className?: string;
  imgSrc?: StaticImageData | string;
}

const Avatar = (props: AvatarProps): JSX.Element => {
  const { className, imgSrc = "" } = props;

  return (
    <Image
      className={`${styles.avatar} ${className}`}
      src={imgSrc}
      alt={`avatar ${imgSrc}`}
    />
  );
};

export default Avatar;
