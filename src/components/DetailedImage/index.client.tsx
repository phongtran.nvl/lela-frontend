import Image, { StaticImageData } from "next/image";
import TextDisplay from "../TextDisplay/index.client";

import styles from "./index.module.scss";

interface DetailedImageProps {
  src: StaticImageData | string;
  imgName?: string;
  imgClassName?: string;
  description?: string;
  desClassName?: string;
  className?: string;
}

const DetailedImage = (props: DetailedImageProps) => {
  const {
    src,
    imgName = "image",
    imgClassName,
    desClassName,
    description,
    className,
    ...rest
  } = props;

  return (
    <div {...rest} className={`${styles.detailedImage} ${className}`}>
      <Image
        src={src}
        alt={imgName}
        className={`${styles.defaultImg} ${imgClassName}`}
      />
      {description && (
        <TextDisplay className={`${styles.defaultImgDes} ${desClassName}`}>
          {description}
        </TextDisplay>
      )}
    </div>
  );
};

export default DetailedImage;
