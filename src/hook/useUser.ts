import {useState} from "react";

type User = {
	fullName: string | null;
	email: string | null
}

export const useUser = () => {
	const [user, setUser] = useState<User>({
		fullName: null,
		email: null,
	});
	return {
		user,
		setUser
	}
}