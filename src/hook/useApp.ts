import {getAllSetting} from "../../api/setting";

export const useApp = async () => {
	const settings = await getAllSetting();
	return {
		settings
	}
}