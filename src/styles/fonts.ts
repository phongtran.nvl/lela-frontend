import { Inter_Tight, Niramit } from "next/font/google";

export const interTight = Inter_Tight({
  style: ["normal", "italic"],
  subsets: ["latin"],
  variable: "--inter-tight",
  display: "swap",
});

export const niramit = Niramit({
  weight: ["300", "400", "500", "600", "700"],
  style: ["normal", "italic"],
  subsets: ["latin"],
  variable: "--niramit",
  display: "swap",
});
