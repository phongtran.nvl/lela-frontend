import * as React from "react";
import { SVGProps } from "react";

interface Props extends SVGProps<SVGSVGElement> {
  color?: string;
  className?: string;
}

const ArrowDown = (props: Props) => {
  const { color = "#101828", className, ...rest } = props;

  return (
    <svg
      className={`arrow-down ${className}`}
      xmlns="http://www.w3.org/2000/svg"
      width={24}
      height={24}
      fill="none"
      {...rest}
    >
      <path
        fill={color}
        fillRule="evenodd"
        d="M12 4a1 1 0 0 1 1 1v11.586l5.293-5.293a1 1 0 0 1 1.414 1.414l-7 7a1 1 0 0 1-1.414 0l-7-7a1 1 0 1 1 1.414-1.414L11 16.586V5a1 1 0 0 1 1-1Z"
        clipRule="evenodd"
      />
    </svg>
  );
};
export default ArrowDown;
