import Image from "next/image";
import dynamic from "next/dynamic";
import get from "lodash/get";
import { redirect } from "next/navigation";

import { getPostDetail, getRelatedPost } from "../../../../api/post";
import ContentBuilderClient from "@/app/post/components/contentBuilder.client";

const Tags = dynamic(() => import("@/components/Tags/index.client"));

const AdBanner = dynamic(() => import("@/components/AdBanner/index.client"));

const AuthorIntroduction = dynamic(() => import("@/components/AuthorIntroduction/index.client"));

const ArticleHeader = dynamic(() => import("@/modules/ArticleHeader/index.client"));

const ArticleRecentlyViewed = dynamic(() => import("@/modules/ArticleRecentlyViewed/index.client"));

const SubscribeBox = dynamic(() => import("@/modules/SubscribeBox/index.client"));

const LalaNews = dynamic(() => import("@/modules/LalaNews/index.client"));

const CoursesAd = dynamic(() => import("@/modules/CoursesAd/index.client"));

const CommentBox = dynamic(() => import("@/modules/CommentBox/index.client"));

import vector2StrokeSvg from "@/assets/svg/vector2Stroke.svg";

import styles from "./page.module.scss";
import "./page-content.scss";
import clsx from "clsx";
import SubscriptionComponent from "@/app/post/components/subscriptionComponent.client";

type PostProps = {
  params: {
    slug: string;
  };
};

export default async function Post({ params }: PostProps) {
  const postDetail = await getPostDetail(params.slug);

  // if (postDetail.statusCode === 404) {
  //   redirect("/404");
  // }

  const relatedPosts = await getRelatedPost(
    get(postDetail, "data.id", ""),
    get(postDetail, "data.mainCategory.id", ""),
    4
  );

  const secondCol = get(postDetail, "data.isLelaTalk", false);

  return (
    <div className={styles.mainArticle}>
      <div className={styles.article}>
        <ArticleHeader
          title={get(postDetail, "data.title", "")}
          description={get(postDetail, "data.description", "")}
          createdBy={get(postDetail, "data.createdBy", {})}
          thumbnail={get(postDetail, "data.thumbnail", "")}
          categoryName={get(postDetail, "data.mainCategory.title", "")}
        />
        <Image
          src={vector2StrokeSvg}
          alt="stroke line"
          className={styles.strokeLine}
        />
        <div
          className={clsx('blogContent', {[styles.doubleCol]: secondCol, [styles.singleCol]: !secondCol })}
        >
          <div className={clsx({[styles.doubleCol1]: secondCol, [styles.singleCol1]: !secondCol})}>
            <div className={styles.content}>
              <ContentBuilderClient content={get(postDetail, "data.content")} />
            </div>
            <SubscriptionComponent/>
            <Tags
              tags={get(postDetail, "data.tags", [])}
              className={styles.tagsWrapper}
            />
            <AuthorIntroduction
              name={get(postDetail, "data.createdBy.fullName", "")}
              brief={get(postDetail, "data.createdBy.description", "")}
              avtSrc={get(postDetail, "data.createdBy.avatar", "")}
              className={styles.authorIntroductionWrapper}
            />
            {
              get(postDetail, "data.isShowComment", true) && (
                <div className={styles.commentBoxWrapper}>
                  <CommentBox />
                </div>
              )
            }
          </div>
          {secondCol && (
            <div className={clsx(styles.col2, {[styles.secondColHidden]: !secondCol})}>
              <ArticleRecentlyViewed />
              <SubscribeBox className={styles.subscribeBoxWrapper} />
            </div>
          )}
        </div>
        <div className={styles.aboveFooterWrapper}>
          {get(relatedPosts, "data", []).length > 0 && (
            <LalaNews relatedPosts={relatedPosts.data || []} />
          )}

          <AdBanner type="external" />
          <CoursesAd />
        </div>
      </div>
    </div>
  );
}
