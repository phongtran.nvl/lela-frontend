'use client'
import {useState} from "react";
import styles from "@/components/ShortQuote/index.module.scss";
import SharePopover from "@/modules/SharePopover/index.client";
import Button from "@/components/Button/index.client";
import clsx from "clsx";


export default function ShareButton() {
	const [popoverOpen, setPopoverOpen] = useState<boolean>(false);
	return (
		<Button
			className={clsx(styles.secondary, 'share-button')}
			button="secondary"
			onClick={() => setPopoverOpen(!popoverOpen)}
		>
			<div className={styles.button}>Chia sẻ</div>
			{popoverOpen && <SharePopover className={styles.sharePopoverWrapper} />}
		</Button>
	)
}