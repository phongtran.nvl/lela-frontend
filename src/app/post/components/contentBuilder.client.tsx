'use client'
import React, {useEffect, useState} from 'react';
import ReactHtmlParser from 'react-html-parser';
import ShareButton from "@/app/post/components/shareButton.client";
import get from "lodash/get";

const transform = (node: any, index: number) => {
	if (get(node, 'attribs.class', null) === 'kg-quote-card-button') {
		// const quote = node.children[0].children[0].children[0].data;
		return (
			<div key={index} className="share-quote-wrapper">
				{/*<p>{quote}</p>*/}
				<ShareButton />
			</div>
		);
	}
	// if (node.parent.attribs.class === 'kg-quote-card') {
	// 	return <ShareButton key={index} />;
	// }
};

type Props = {
	content: any
}

export default function ContentBuilderClient({content}: Props) {
	const [contentReady, setContentReady] = useState<boolean>(false);
	useEffect(() => {
		setContentReady(true);
	}, []);
	const contentBuild = ReactHtmlParser(content, {transform});
	if (!contentReady) return null;
	return <React.Fragment>{contentBuild}</React.Fragment>
}