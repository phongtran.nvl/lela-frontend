'use client';
import clsx from "clsx";
import styles from './subscription.module.scss';
import vector2 from '@/assets/svg/vector2.svg';
import Image from "next/image";

export default function SubscriptionComponent() {
	return (
		<>
			<div className={styles.subscriptionWrapper}>
				<p className={styles.title}>
					Bạn thấy bài viết thú vị và muốn đọc thêm về LeLa News?
				</p>
				<p className={styles.descriptionContent}>
					LeLa News là series nói về kinh doanh, khởi nghiệp, kinh nghiệm sống và những cuộc trò chuyện truyền cảm hứng.
				</p>
				<div className={styles.buttonWrapper}>
					<button className={clsx(styles.button, styles.button__more)}>Khám phá thêm</button>
					<button className={clsx(styles.button, styles.button__follow)}>Theo dõi ngay</button>
				</div>
			</div>
			<div className="w-full">
				<Image className="w-full" src={vector2} alt="vector"/>
			</div>
		</>
	)
}