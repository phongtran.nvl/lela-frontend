import dynamic from "next/dynamic";
import Image from "next/image";

const AdBanner = dynamic(() => import("@/components/AdBanner/index.client"));

const Highlight = dynamic(() => import("@/modules/Highlight/index.client"));

const RecentlyViewedPosts = dynamic(() => import("@/modules/RecentlyViewedPosts"));
const DateLatestPosts = dynamic(() => import("@/modules/DateLatestPosts"));
const SubscribeBoxHome = dynamic(() => import("@/modules/SubscribeBoxHome/index.client"));
const CategoryLatestPosts = dynamic(() => import("@/modules/CategoryLatestPosts"));
const Series = dynamic(() => import("@/modules/Series"), {});
const EditorRecommendation = dynamic(() => import("@/modules/EditorRecommendation"));
const CoursesAd = dynamic(() => import("@/modules/CoursesAd/index.client"));

import vector2StrokeSvg from "@/assets/svg/vector2Stroke.svg";

import styles from "./page.module.scss";
import {useApp} from "@/hook/useApp";
import getSettingValue from "@/utils/setting";

export default async function Home() {
  const app = await useApp();
  const isShowSeries = getSettingValue(app.settings, 'is_show_series') === '1';

  return (
    <main className={styles.home}>
      <div>
        <Highlight />
        <RecentlyViewedPosts />
        <DateLatestPosts />
        <AdBanner />
        <div className={styles.graphicKv}>
          <Image
            className={styles.vector2Stroke1}
            alt="stroke like"
            src={vector2StrokeSvg}
          />
        </div>
        <SubscribeBoxHome className={styles.homeSubscribeBoxWrapper} />
        <CategoryLatestPosts />
        {
          isShowSeries && <Series />
        }
        <EditorRecommendation />
        <CoursesAd />
      </div>
    </main>
  );
}
