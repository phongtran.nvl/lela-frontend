import React from "react";
const AdBanner = dynamic(() => import("@/components/AdBanner/index.client"));
const StickyNavbar = dynamic(() => import("@/components/StickyNavbar/index.client"));
const ScrollToTop = dynamic(
  () => import("@/components/ScrollToTop/index.client")
);
const Footer = dynamic(() => import("@/components/Footer"), {
  suspense: true,
});
import { StyledEngineProvider } from "@mui/material/styles";
import "./globals.scss";
import styles from "@/app/page.module.scss";
import dynamic from "next/dynamic";
import { Metadata } from "next";

export const metadata: Metadata = {
  title: "LeLa Journal",
  description: "LeLa Journal",
};
export default async function RootLayout({
  children,
}: {
  children: React.ReactNode;
}) {

  return (
    <html lang="vi">
      <body>
        <StyledEngineProvider injectFirst>
					<div className={styles.wrapper__adBanner}>
						<AdBanner />
					</div>
					<div className={styles.wrapper__stickyNavbar}>
						<StickyNavbar />
					</div>
					{children}
					<ScrollToTop />
					<div className={styles.wrapper__footer}>
						<Footer />
					</div>
        </StyledEngineProvider>
      </body>
    </html>
  );
}
