/** @type {import('next').NextConfig} */
const path = require("path");

const nextConfig = {
  images: {
    remotePatterns: [
      {
        protocol: "https",
        hostname: "lela2023.s3.ap-southeast-1.amazonaws.com",
        port: "",
        pathname: "**",
      },
      {
        protocol: "https",
        hostname: "s3.ap-southeast-1.amazonaws.com",
        port: "",
        pathname: "**",
      },
    ],
  },
  sassOptions: {
    includePaths: [path.join()],
  },
};

module.exports = nextConfig;
